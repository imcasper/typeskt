
1.9.2
- New color model
- LRU-Cache
- DoublyLinkedList

1.9.1
- Triangulator: Round rect added
- Triangulator: Polygon optimized
- Fix: camera zoom (not inverted now)
- Color by one value 

1.9.0
- Some vector functions are slightly optimized
- Viewport use floats now
- Matrix compose
- Can read / write numbers from ByteArray
- Intersection - add symmetric function

1.8.1
- color to hex

1.8.0
- use package `ru.casperix.math`
- back to java 17

1.7.8
- auto deploy
 
1.5.2
- change color type
