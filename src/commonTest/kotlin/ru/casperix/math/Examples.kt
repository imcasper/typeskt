package ru.casperix.math

import ru.casperix.math.color.Color
import ru.casperix.math.geometry.Line2f
import ru.casperix.math.geometry.Quad2f
import ru.casperix.math.interpolation.float32.InterpolationFloat
import ru.casperix.math.interpolation.float32.cosineInterpolatef
import ru.casperix.math.intersection.float32.Intersection2Float
import ru.casperix.math.vector.float32.Vector2f

/**
 *
 */
object Examples {
    fun intersection() {
        val intersection = Intersection2Float.getSegmentWithSegment(
            Line2f(Vector2f(0f, 0f), Vector2f(2f, 2f)),
            Line2f(Vector2f(2f, 0f), Vector2f(0f, 2f)),
        )
        //  intersection == Vector2f(1f, 1f)

        val hasIntersection = Intersection2Float.hasPointWithQuad(
            Vector2f.ONE,
            Quad2f(
                Vector2f(0f, 0f),
                Vector2f(2f, 0f),
                Vector2f(2f, 2f),
                Vector2f(0f, 2f),
            )
        )
//  hasIntersection == true

    }

    fun interpolation() {
        InterpolationFloat.color(Color.RED, Color.GREEN, 0.5f, cosineInterpolatef)
    }
}