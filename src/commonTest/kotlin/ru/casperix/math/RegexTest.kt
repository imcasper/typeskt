package ru.casperix.math

import kotlin.test.Test

class RegexTest {

	@Test
	fun test() {
		val byWordSplitter = Regex("[\\p{Lu}][\\p{Ll}]*")
//		val byWordSplitter = Regex("([A-Z])([a-z])")

		byWordSplitter.findAll("HelloWorld").map { it.value }.forEach {
			println(it)
		}
	}
}