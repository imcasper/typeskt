package ru.casperix.math.curve

import ru.casperix.math.curve.float32.LineCurve2f
import ru.casperix.math.test.FloatTest
import ru.casperix.math.vector.float32.Vector2f
import kotlin.test.Test
import kotlin.test.assertEquals

class CurveSplitTest {
    @Test
    fun test() {

        val list = LineCurve2f(Vector2f(0f), Vector2f(10f, 0f))

        val actuals = list.split(listOf(0.2f, 0.6f, 0.8f))

        assertEquals(4, actuals.size)

        FloatTest.assertEquals(Vector2f(0f, 0f), actuals[0].start)
        FloatTest.assertEquals(Vector2f(2f, 0f), actuals[0].finish)

        FloatTest.assertEquals(Vector2f(2f, 0f), actuals[1].start)
        FloatTest.assertEquals(Vector2f(6f, 0f), actuals[1].finish)

        FloatTest.assertEquals(Vector2f(6f, 0f), actuals[2].start)
        FloatTest.assertEquals(Vector2f(8f, 0f), actuals[2].finish)

        FloatTest.assertEquals(Vector2f(8f, 0f), actuals[3].start)
        FloatTest.assertEquals(Vector2f(10f, 0f), actuals[3].finish)
    }
}