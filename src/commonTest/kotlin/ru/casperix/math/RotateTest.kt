package ru.casperix.math

import ru.casperix.math.vector.int32.Vector2i
import ru.casperix.math.vector.rotateCCW
import ru.casperix.math.vector.rotateCW
import kotlin.test.Test
import kotlin.test.assertEquals

class RotateTest {

	@Test
	fun test() {

		assertEquals( Vector2i.X.rotateCCW(), Vector2i.Y)
		assertEquals( Vector2i.X.rotateCW(), -Vector2i.Y)

		assertEquals( Vector2i.Y.rotateCCW(), -Vector2i.X)
		assertEquals( Vector2i.Y.rotateCW(), Vector2i.X)

		assertEquals(Vector2i.ONE.rotateCW().rotateCW(), -Vector2i.ONE)
		assertEquals(Vector2i.ONE.rotateCCW().rotateCCW(), -Vector2i.ONE)
	}
}