package ru.casperix.math.geometry.iterator

import ru.casperix.math.geometry.Line
import ru.casperix.math.geometry.Line2d
import ru.casperix.math.geometry.delta
import ru.casperix.math.iteration.Line2Iterator
import ru.casperix.math.random.nextVector2d
import ru.casperix.math.vector.float64.Vector2d
import ru.casperix.math.vector.int32.Vector2i
import kotlin.math.roundToInt
import kotlin.random.Random
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue


class LineIteratorTest {
    @Test
    fun testSimple() {
        val random = Random(0)

        val range2 = 2000.0
        for (i in 1..10) {
            val line = Line2d(
                random.nextVector2d(-Vector2d(range2), Vector2d(range2)),
                random.nextVector2d(-Vector2d(range2), Vector2d(range2))
            )

            testLine(i, line, 1.0)
        }

        val range = 10.0
        for (i in 1..10000) {
            val line = Line2d(
                random.nextVector2d(-Vector2d(range), Vector2d(range)),
                random.nextVector2d(-Vector2d(range), Vector2d(range))
            )

            testLine(i, line, 1.0)
        }

    }

    private fun testLine(step: Int, line: Line<Vector2d>, divideFactor: Double) {

        val manualPoints = mutableSetOf<Vector2i>()

        val steps = 1 + (line.delta().length() * divideFactor).roundToInt()
        val stepsD = steps.toDouble()

        assertTrue("Can't check line") { steps <= 1024 * 1024 * 1024 }
        println("phase: $step; steps: $steps")

        for (lineStep in 0..steps) {
            val point = line.v0 * (stepsD - lineStep.toDouble()) / stepsD + line.v1 * lineStep.toDouble() / stepsD
            manualPoints.add((point - Vector2d.HALF).roundToVector2i()!!)
        }

        val temp = mutableListOf<Vector2i>()
        Line2Iterator.iterate(line) { temp.add(it); true }
        val resultPoints = temp.toSet()

        assertTrue { resultPoints.size == temp.size }
        assertTrue { resultPoints.size >= manualPoints.size }

        if (resultPoints.size > manualPoints.size) {
            testLine(step, line, divideFactor * 4.0)
            return
        } else {
            assertEquals(manualPoints, resultPoints)
        }
    }
}