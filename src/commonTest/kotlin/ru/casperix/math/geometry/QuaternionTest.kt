package ru.casperix.math.geometry

import ru.casperix.math.quaternion.float64.QuaternionDouble
import ru.casperix.math.quaternion.float32.QuaternionFloat
import ru.casperix.math.transform.float64.Transform3d
import ru.casperix.math.vector.float64.Vector3d
import ru.casperix.math.vector.float32.Vector3f
import kotlin.math.PI
import kotlin.random.Random
import kotlin.test.Test
import kotlin.test.assertEquals

class QuaternionTest {
	@Test
	fun testAxisAngle() {
		val XtoX = QuaternionDouble.fromAxisAnge(Vector3d.X, PI / 2).transform(Vector3d.X)
		assertEquals(Vector3d.X.toPrecision(16), XtoX.toPrecision(16), "X around X")

		val XtoY = QuaternionDouble.fromAxisAnge(Vector3d.X, PI / 2).transform(Vector3d.Y)
		assertEquals(Vector3d.Z.toPrecision(16), XtoY.toPrecision(16), "X around Y")

		val XtoZ = QuaternionDouble.fromAxisAnge(Vector3d.X, -PI / 2).transform(Vector3d.Z)
		assertEquals(Vector3d.Y.toPrecision(16), XtoZ.toPrecision(16), "X around Z")
	}

//	@Test
	fun testEulerD() {
		var source = QuaternionDouble.fromAxisAnge(Vector3d(3.0, 2.0, 1.0), PI / 4)
		val euler = source.toEuler()
		var target = QuaternionDouble.fromEulerAngles(euler)
		assertEquals(source.toPrecision(4), target.toPrecision(4))
	}

//	@Test
	fun testEulerF() {
		val source = QuaternionFloat.fromAxisAnge(Vector3f(3f, 2f, 1f), PI.toFloat() / 4)
		val euler = source.toEuler()
		val target = QuaternionFloat.fromEulerAngles(euler)
		assertEquals(source.toPrecision(4), target.toPrecision(4))
	}

	@Test
	fun testSimple() {
		val source = Transform3d(Vector3d.ONE, Vector3d.ONE, QuaternionDouble.IDENTITY)
		assertEquals(Vector3d.Z, source.getLocalZ())
		assertEquals(Vector3d.Y, source.getLocalY())
		assertEquals(Vector3d.X, source.getLocalX())
	}

	@Test
	fun testBasisSimple() {
		val a = QuaternionDouble.fromYZ(Vector3d.X, Vector3d.Y)

		val fwA = Transform3d.getLocalY(a)
		val upA = Transform3d.getLocalZ(a)
		assertEquals(Vector3d.X, fwA)
		assertEquals(Vector3d.Y, upA)
	}

	@Test
	fun testBasisCustom() {
		val random = Random(0)

		//	generate random basis list (forward & up -- orthogonal )
		val basisList = mutableListOf<Pair<Vector3d, Vector3d>>()
		for (i in 1..1000) {
			val forward = Vector3d(random.nextDouble(), random.nextDouble(), random.nextDouble())
			val right = Vector3d(random.nextDouble(), random.nextDouble(), random.nextDouble())
			val up = right.cross(forward)
			basisList.add(Pair(forward.normalize(), up.normalize()))
		}

		//	check what generated correct Quaternion for next basis
		basisList.forEach {
			val forward = it.first
			val up = it.second

			val T = QuaternionDouble.fromYZ(forward, up)
			val forwardT = Transform3d.getLocalY(T)
			val upT = Transform3d.getLocalZ(T)

			assertEquals(forward.toPrecision(8), forwardT.toPrecision(8), "FWD q=$T; pair=$it; ")
			assertEquals(up.toPrecision(8), upT.toPrecision(8), "UP q=$T; pair=$it; ")
		}
	}
}