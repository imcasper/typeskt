package ru.casperix.math.geometry

import ru.casperix.math.intersection.int32.Intersection2Int
import ru.casperix.math.vector.int32.Vector2i
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class Intersection2iTest {
    @Test
    fun pointWithLine2() {
        val line = Line2i(Vector2i(4, 0), Vector2i(8, 4))

        (-2..10).forEach {
            val p = Vector2i(4 + it, it)
            val expected = (it in 0..4)
            assertEquals(expected, Intersection2Int.hasPointWithLine(p, line), "point: $p with line ${line.v0} => ${line.v1}")
        }
    }
    @Test
    fun pointWithLine() {
        val line = Line2i(Vector2i(0, 0), Vector2i(4, 2))
        val insideList = listOf(Vector2i(0, 0), Vector2i(2, 1), Vector2i(4, 2))

        val outsideList = listOf(
            Vector2i(-1,-1),
            Vector2i(0, -1),
            Vector2i(1, -1),
            Vector2i(2, -1),
            Vector2i(3, -1),
            Vector2i(4, -1),
            Vector2i(5, -1),

            Vector2i(-1, 0),
            Vector2i(1, 0),
            Vector2i(2, 0),
            Vector2i(3, 0),
            Vector2i(4, 0),
            Vector2i(5, 0),

            Vector2i(-1, 1),
            Vector2i(0, 1),
            Vector2i(1, 1),
            Vector2i(3, 1),
            Vector2i(4, 1),
            Vector2i(5, 1),

            Vector2i(-1, 2),
            Vector2i(0, 2),
            Vector2i(1, 2),
            Vector2i(2, 2),
            Vector2i(3, 2),
            Vector2i(5, 2),

            Vector2i(-1, 3),
            Vector2i(0, 3),
            Vector2i(1, 3),
            Vector2i(2, 3),
            Vector2i(3, 3),
            Vector2i(4, 3),
            Vector2i(5, 3),
            )

        insideList.forEach { inside ->
            assertTrue(Intersection2Int.hasPointWithLine(inside, line), "Expected what point $inside inside line $line")
        }
        outsideList.forEach { outside ->
            assertFalse(Intersection2Int.hasPointWithLine(outside, line), "Expected what point $outside outside line $line")
        }
    }

    @Test
    fun pointWithTriangle() {
        val triangle = Triangle2i(Vector2i(0, 0), Vector2i(2, 0), Vector2i(0, 2))

        val insideList = listOf(
            Vector2i(0, 0),
            Vector2i(1, 0),
            Vector2i(2, 0),
            Vector2i(0, 1),
            Vector2i(1, 1),
            Vector2i(0, 2),
        )

        val outsideList = listOf(
            Vector2i(-1, -1),

            Vector2i(-1, 0),
            Vector2i(-1, 1),
            Vector2i(-1, 2),
            Vector2i(-1, 3),

            Vector2i(0, -1),
            Vector2i(1, -1),
            Vector2i(2, -1),
            Vector2i(3, -1),

            Vector2i(3, 0),
            Vector2i(2, 1),
            Vector2i(1, 2),
            Vector2i(0, 3),
        )


        insideList.forEach { inside ->
            assertTrue(Intersection2Int.hasPointWithTriangle(inside, triangle), "expected inside point: $inside")
        }
        outsideList.forEach { outside ->
            assertFalse(Intersection2Int.hasPointWithTriangle(outside, triangle), "expected outside point: $outside")
        }
    }

    @Test
    fun pointWithQuad() {
        val quad = Quad2i(Vector2i(0, -2), Vector2i(2, 0), Vector2i(0, 2), Vector2i(-2, 0))

        val insideList = listOf(
            Vector2i(0, 0),
            Vector2i(0, -2),
            Vector2i(2, 0),
            Vector2i(0, 2),
            Vector2i(-2, 0),

            Vector2i(-1, -1),
            Vector2i(-1, 0),
            Vector2i(-1, 1),
            Vector2i(1, -1),

            Vector2i(1, 0),
            Vector2i(1, 1),
            Vector2i(0, 1),
            Vector2i(0, -1),
        )

        val outsideList = listOf(
            Vector2i(-2, -2),
            Vector2i(-2, -1),
            Vector2i(-1, -2),

            Vector2i(2, 2),
            Vector2i(2, 1),
            Vector2i(1, 2),

            Vector2i(2, -2),
            Vector2i(2, -1),
            Vector2i(1, -2),

            Vector2i(-2, 2),
            Vector2i(-2, 1),
            Vector2i(-1, 2),
        )

        insideList.forEach { inside ->
            assertTrue(Intersection2Int.hasPointWithQuad(inside, quad), "expected inside point: $inside")
        }
        outsideList.forEach { outside ->
            assertFalse(Intersection2Int.hasPointWithQuad(outside, quad), "expected outside point: $outside")
        }
    }
}