package ru.casperix.math.geometry

import ru.casperix.math.intersection.float64.Intersection2Double
import ru.casperix.math.vector.float64.Vector2d
import kotlin.test.Test
import kotlin.test.assertEquals

class Intersection2dTest {
    val smallValue = 0.000001

    @Test
    fun pointWithTriangle() {
        val testTriangle = Triangle2d(
            Vector2d(1.0, 1.0),
            Vector2d(3.0, 1.0),
            Vector2d(1.0, 3.0),
        )

        assertEquals(false, Intersection2Double.hasPointWithTriangle(Vector2d(1.0 - smallValue, 1.0), testTriangle))
        assertEquals(false, Intersection2Double.hasPointWithTriangle(Vector2d(1.0, 1.0 - smallValue), testTriangle))
        assertEquals(false, Intersection2Double.hasPointWithTriangle(Vector2d(1.0 - smallValue), testTriangle))

        assertEquals(true, Intersection2Double.hasPointWithTriangle(Vector2d(1.0, 1.0+smallValue), testTriangle))
        assertEquals(true, Intersection2Double.hasPointWithTriangle(Vector2d(1.0+smallValue, 1.0), testTriangle))
        assertEquals(true, Intersection2Double.hasPointWithTriangle(Vector2d(1.0+smallValue), testTriangle))

        assertEquals(true, Intersection2Double.hasPointWithTriangle(Vector2d(3.0 - smallValue, 1.0), testTriangle))
        assertEquals(false, Intersection2Double.hasPointWithTriangle(Vector2d(3.0 + smallValue, 1.0), testTriangle))

        assertEquals(true, Intersection2Double.hasPointWithTriangle(Vector2d(1.0, 3.0 - smallValue), testTriangle))
        assertEquals(false, Intersection2Double.hasPointWithTriangle(Vector2d(1.0, 3.0 + smallValue), testTriangle))

        assertEquals(true, Intersection2Double.hasPointWithTriangle(Vector2d(2.0 - smallValue), testTriangle))
        assertEquals(false, Intersection2Double.hasPointWithTriangle(Vector2d(2.0 + smallValue), testTriangle))
    }

    @Test
    fun pointWithQuad() {
        val testQuad = Quad2d(
            Vector2d(1.0, 1.0),
            Vector2d(3.0, 1.0),
            Vector2d(3.0, 3.0),
            Vector2d(1.0, 3.0),
        )

        assertEquals(false, Intersection2Double.hasPointWithQuad(Vector2d(1.0 - smallValue), testQuad))
        assertEquals(false, Intersection2Double.hasPointWithQuad(Vector2d(3.0 + smallValue), testQuad))
        assertEquals(false, Intersection2Double.hasPointWithQuad(Vector2d(3.0, 1.0-smallValue), testQuad))
        assertEquals(false, Intersection2Double.hasPointWithQuad(Vector2d(1.0-smallValue, 3.0), testQuad))

        assertEquals(true, Intersection2Double.hasPointWithQuad(Vector2d(1.0, 1.0), testQuad))
        assertEquals(true, Intersection2Double.hasPointWithQuad(Vector2d(2.0, 2.0), testQuad))
        assertEquals(true, Intersection2Double.hasPointWithQuad(Vector2d(3.0, 3.0), testQuad))

        assertEquals(true, Intersection2Double.hasPointWithQuad(Vector2d(1.0, 3.0), testQuad))
        assertEquals(true, Intersection2Double.hasPointWithQuad(Vector2d(2.0, 2.0), testQuad))
        assertEquals(true, Intersection2Double.hasPointWithQuad(Vector2d(3.0, 1.0), testQuad))

    }
}