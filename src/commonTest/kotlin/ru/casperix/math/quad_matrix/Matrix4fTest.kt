package ru.casperix.math.quad_matrix

import ru.casperix.math.quad_matrix.float32.Matrix4f
import ru.casperix.math.test.FloatTest
import ru.casperix.math.vector.float32.Vector3f
import kotlin.test.Test
import kotlin.test.assertEquals

class Matrix4fTest {
    @Test
    fun transpose() {
        val source = Matrix4f(FloatArray(16) { it.toFloat() })

        assertEquals(
            Matrix4f(
                floatArrayOf(
                    0f, 4f, 8f, 12f,
                    1f, 5f, 9f, 13f,
                    2f, 6f, 10f, 14f,
                    3f, 7f, 11f, 15f
                )
            ),
            source.transpose()
        )
    }

    @Test
    fun mul() {
        val m1 = Matrix4f(FloatArray(16) { (it + 1).toFloat() })
        val m2 = Matrix4f(FloatArray(16) { (it + 17).toFloat() })

        val expected = Matrix4f(
            floatArrayOf(
                250f, 260f, 270f, 280f,
                618f, 644f, 670f, 696f,
                986f, 1028f, 1070f, 1112f,
                1354f, 1412f, 1470f, 1528f,
            )
        )
        val actual = m1 * m2

        FloatTest.assertEquals(expected, actual)
    }

    @Test
    fun translate() {
        val source = Matrix4f(FloatArray(16) { it.toFloat() })

        val actual = source.setTranslate(Vector3f(-1f, -2f, -3f))
        val expected = Matrix4f(floatArrayOf(0f, 1f, 2f, 3f, 4f, 5f, 6f, 7f, 8f, 9f, 10f, 11f, -1f, -2f, -3f, 15f))
        assertEquals(expected, actual)
    }
}