package ru.casperix.math

import ru.casperix.math.angle.float32.RadianFloat
import ru.casperix.math.test.FloatTest
import kotlin.test.Test

class RadianTest {
    @Test
    fun absTest() {
        FloatTest.assertEquals(0.9f, RadianFloat.absMod(0.9f, 1.0f))
        FloatTest.assertEquals(0.9f, RadianFloat.absMod(-0.1f, 1.0f))
        FloatTest.assertEquals(0.9f, RadianFloat.absMod(1.9f, 1.0f))
        FloatTest.assertEquals(0.9f, RadianFloat.absMod(-1.1f, 1.0f))
        FloatTest.assertEquals(0.9f, RadianFloat.absMod(1000.9f, 1.0f))
        FloatTest.assertEquals(0.9f, RadianFloat.absMod(-1000.1f, 1.0f))
        FloatTest.assertEquals(0.0f, RadianFloat.absMod(2.0f, 1.0f))
        FloatTest.assertEquals(0.0f, RadianFloat.absMod(1.0f, 1.0f))
        FloatTest.assertEquals(0.0f, RadianFloat.absMod(0.0f, 1.0f))
        FloatTest.assertEquals(0.0f, RadianFloat.absMod(-1.0f, 1.0f))
        FloatTest.assertEquals(0.0f, RadianFloat.absMod(-2.0f, 1.0f))
    }
}