package ru.casperix.math.format

import ru.casperix.math.color.rgba.RgbaColor4d
import kotlin.test.Test
import kotlin.test.assertEquals

class ColorFormatTest {

	@Test
	fun toHexString() {
		assertEquals("000102fe", RgbaColor4d(0.0, 1.0 / 255.0, 2.0 / 255.0, 254.0 / 255.0).toHexString())
		assertEquals("80808080", RgbaColor4d(0.5, 0.5, 0.5, 0.5).toHexString())
		assertEquals("ffffffff", RgbaColor4d(1.0, 1.0, 1.0, 1.0).toHexString())
	}
}