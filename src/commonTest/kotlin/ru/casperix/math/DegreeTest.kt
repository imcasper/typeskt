package ru.casperix.math

import ru.casperix.math.angle.float32.DegreeFloat
import ru.casperix.math.angle.float32.RadianFloat
import ru.casperix.math.geometry.fDEGREE_TO_RADIAN
import ru.casperix.math.geometry.fRADIAN_TO_DEGREE
import ru.casperix.math.polar.float32.PolarCoordinateFloat
import ru.casperix.math.random.nextRadianFloat
import ru.casperix.math.test.FloatCompare
import ru.casperix.math.test.FloatTest
import kotlin.math.absoluteValue
import kotlin.math.roundToInt
import kotlin.random.Random
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class DegreeTest {
    @Test
    fun degreeToRadian() {
        val random = Random(1)
        val values = List(100) { random.nextRadianFloat()}

        values.map {
            val degree = it.toDegree()

                val radian = degree.toRadian()

            FloatTest.assertEquals(it, radian)
            FloatTest.assertEquals(degree.value * fDEGREE_TO_RADIAN, radian.value)
        }
    }

    @Test
    fun betweenTest() {
        FloatTest.assertEquals(
            ru.casperix.math.angle.float32.DegreeFloat(180f), ru.casperix.math.angle.float32.DegreeFloat.betweenAngles(
                ru.casperix.math.angle.float32.DegreeFloat(
                    0f
                ), ru.casperix.math.angle.float32.DegreeFloat(180f)
            ))
        FloatTest.assertEquals(
            ru.casperix.math.angle.float32.DegreeFloat(180f), ru.casperix.math.angle.float32.DegreeFloat.betweenAngles(
                ru.casperix.math.angle.float32.DegreeFloat(
                    180f
                ), ru.casperix.math.angle.float32.DegreeFloat(360f)
            ))
        FloatTest.assertEquals(
            ru.casperix.math.angle.float32.DegreeFloat(180f), ru.casperix.math.angle.float32.DegreeFloat.betweenAngles(
                ru.casperix.math.angle.float32.DegreeFloat(
                    360f
                ), ru.casperix.math.angle.float32.DegreeFloat(-180f)
            ))
        FloatTest.assertEquals(
            ru.casperix.math.angle.float32.DegreeFloat(90f), ru.casperix.math.angle.float32.DegreeFloat.betweenAngles(
                ru.casperix.math.angle.float32.DegreeFloat(
                    90f
                ), ru.casperix.math.angle.float32.DegreeFloat(0f)
            ))
        FloatTest.assertEquals(
            ru.casperix.math.angle.float32.DegreeFloat(90f), ru.casperix.math.angle.float32.DegreeFloat.betweenAngles(
                ru.casperix.math.angle.float32.DegreeFloat(
                    -90f
                ), ru.casperix.math.angle.float32.DegreeFloat(0f)
            ))
        FloatTest.assertEquals(
            ru.casperix.math.angle.float32.DegreeFloat(90f), ru.casperix.math.angle.float32.DegreeFloat.betweenAngles(
                ru.casperix.math.angle.float32.DegreeFloat(
                    90f
                ), ru.casperix.math.angle.float32.DegreeFloat(360f)
            ))
        FloatTest.assertEquals(
            ru.casperix.math.angle.float32.DegreeFloat(90f), ru.casperix.math.angle.float32.DegreeFloat.betweenAngles(
                ru.casperix.math.angle.float32.DegreeFloat(
                    -90f
                ), ru.casperix.math.angle.float32.DegreeFloat(360f)
            ))
        FloatTest.assertEquals(
            ru.casperix.math.angle.float32.DegreeFloat(90f), ru.casperix.math.angle.float32.DegreeFloat.betweenAngles(
                ru.casperix.math.angle.float32.DegreeFloat(
                    270f
                ), ru.casperix.math.angle.float32.DegreeFloat(0f)
            ))
        FloatTest.assertEquals(
            ru.casperix.math.angle.float32.DegreeFloat(90f), ru.casperix.math.angle.float32.DegreeFloat.betweenAngles(
                ru.casperix.math.angle.float32.DegreeFloat(
                    -270f
                ), ru.casperix.math.angle.float32.DegreeFloat(0f)
            ))
        FloatTest.assertEquals(
            ru.casperix.math.angle.float32.DegreeFloat(0f), ru.casperix.math.angle.float32.DegreeFloat.betweenAngles(
                ru.casperix.math.angle.float32.DegreeFloat(
                    0f
                ), ru.casperix.math.angle.float32.DegreeFloat(360f)
            ))
        FloatTest.assertEquals(
            ru.casperix.math.angle.float32.DegreeFloat(0f), ru.casperix.math.angle.float32.DegreeFloat.betweenAngles(
                ru.casperix.math.angle.float32.DegreeFloat(
                    360f
                ), ru.casperix.math.angle.float32.DegreeFloat(0f)
            ))
        FloatTest.assertEquals(
            ru.casperix.math.angle.float32.DegreeFloat(0f), ru.casperix.math.angle.float32.DegreeFloat.betweenAngles(
                ru.casperix.math.angle.float32.DegreeFloat(
                    180f
                ), ru.casperix.math.angle.float32.DegreeFloat(-180f)
            ))
        FloatTest.assertEquals(
            ru.casperix.math.angle.float32.DegreeFloat(0f), ru.casperix.math.angle.float32.DegreeFloat.betweenAngles(
                ru.casperix.math.angle.float32.DegreeFloat(
                    0f
                ), ru.casperix.math.angle.float32.DegreeFloat(-0f)
            ))
        FloatTest.assertEquals(
            ru.casperix.math.angle.float32.DegreeFloat(0f), ru.casperix.math.angle.float32.DegreeFloat.betweenAngles(
                ru.casperix.math.angle.float32.DegreeFloat(
                    90f
                ), ru.casperix.math.angle.float32.DegreeFloat(-270f)
            ))
        FloatTest.assertEquals(
            ru.casperix.math.angle.float32.DegreeFloat(0f), ru.casperix.math.angle.float32.DegreeFloat.betweenAngles(
                ru.casperix.math.angle.float32.DegreeFloat(
                    270f
                ), ru.casperix.math.angle.float32.DegreeFloat(-90f)
            ))
    }


    @Test
    fun interpolateTest() {

        repeat(72) {
            val angle = ru.casperix.math.angle.float32.DegreeFloat((it - 18) * 10f)

            val min = angle - 80f
            val max = angle + 80f

            assertEquals(angle.normalize(), ru.casperix.math.angle.float32.DegreeFloat.interpolateAngular(min, max, 0.5f))
            assertEquals(angle.normalize(), ru.casperix.math.angle.float32.DegreeFloat.interpolateAngular(max, min, 0.5f))
            assertEquals(min.normalize(), ru.casperix.math.angle.float32.DegreeFloat.interpolateAngular(min, max, 0.0f))
            assertEquals(max.normalize(), ru.casperix.math.angle.float32.DegreeFloat.interpolateAngular(min, max, 1.0f))
        }

        assertEquals(
            ru.casperix.math.angle.float32.DegreeFloat(210f), ru.casperix.math.angle.float32.DegreeFloat.interpolateAngular(
                ru.casperix.math.angle.float32.DegreeFloat(
                    -180f
                ), ru.casperix.math.angle.float32.DegreeFloat(-120f), 0.5f))
        assertEquals(
            ru.casperix.math.angle.float32.DegreeFloat(210f), ru.casperix.math.angle.float32.DegreeFloat.interpolateAngular(
                ru.casperix.math.angle.float32.DegreeFloat(
                    -120f
                ), ru.casperix.math.angle.float32.DegreeFloat(-180f), 0.5f))
        assertEquals(
            ru.casperix.math.angle.float32.DegreeFloat(300f), ru.casperix.math.angle.float32.DegreeFloat.interpolateAngular(
                ru.casperix.math.angle.float32.DegreeFloat(
                    0f
                ), ru.casperix.math.angle.float32.DegreeFloat(-120f), 0.5f))
        assertEquals(
            ru.casperix.math.angle.float32.DegreeFloat(300f), ru.casperix.math.angle.float32.DegreeFloat.interpolateAngular(
                ru.casperix.math.angle.float32.DegreeFloat(
                    0f
                ), ru.casperix.math.angle.float32.DegreeFloat(240f), 0.5f))
        assertEquals(
            ru.casperix.math.angle.float32.DegreeFloat(300f), ru.casperix.math.angle.float32.DegreeFloat.interpolateAngular(
                ru.casperix.math.angle.float32.DegreeFloat(
                    240f
                ), ru.casperix.math.angle.float32.DegreeFloat(0f), 0.5f))
        assertEquals(
            ru.casperix.math.angle.float32.DegreeFloat(300f), ru.casperix.math.angle.float32.DegreeFloat.interpolateAngular(
                ru.casperix.math.angle.float32.DegreeFloat(
                    -120f
                ), ru.casperix.math.angle.float32.DegreeFloat(0f), 0.5f))
        assertEquals(
            ru.casperix.math.angle.float32.DegreeFloat(60f), ru.casperix.math.angle.float32.DegreeFloat.interpolateAngular(
                ru.casperix.math.angle.float32.DegreeFloat(
                    0f
                ), ru.casperix.math.angle.float32.DegreeFloat(120f), 0.5f))
        assertEquals(
            ru.casperix.math.angle.float32.DegreeFloat(120f), ru.casperix.math.angle.float32.DegreeFloat.interpolateAngular(
                ru.casperix.math.angle.float32.DegreeFloat(
                    30f
                ), ru.casperix.math.angle.float32.DegreeFloat(210f), 0.5f))
        assertEquals(
            ru.casperix.math.angle.float32.DegreeFloat(280f), ru.casperix.math.angle.float32.DegreeFloat.interpolateAngular(
                ru.casperix.math.angle.float32.DegreeFloat(
                    200f
                ), ru.casperix.math.angle.float32.DegreeFloat(0f),0.5f))
        assertEquals(
            ru.casperix.math.angle.float32.DegreeFloat(10f), ru.casperix.math.angle.float32.DegreeFloat.interpolateAngular(
                ru.casperix.math.angle.float32.DegreeFloat(
                    290f
                ), ru.casperix.math.angle.float32.DegreeFloat(90f), 0.5f))
    }

    @Test
    fun normalizeTest() {
        listOf(0, 60, 120, 180, 240, 270).map { ru.casperix.math.angle.float32.DegreeFloat(it.toFloat()) }.forEach { expected ->
            listOf(
                expected,
                expected - 720f,
                expected - 360f,
                expected + 360f,
                expected + 720f,
            ).forEach { degree ->
                val angle = degree.normalize()
                assertEquals(expected, angle)
            }
        }
    }

    @Test
    fun getAngleTest() {
        (0..36).forEach { angleIndex ->
            val inputDegree = ru.casperix.math.angle.float32.DegreeFloat(angleIndex * 10f)
            val radian = inputDegree.toRadian()

            val polar = PolarCoordinateFloat(1f, radian)
            val decart = polar.toDecart()

            val outputDegree = RadianFloat.byDirection(decart) .toDegree()

            println("$inputDegree -> ${decart.toPrecision(2)} -> $outputDegree")

        }
    }

    fun assertEquals(expected: Float, actual: Float) {
        assertTrue(almostEqual(expected, actual), "Expected that $expected equal $actual")
    }


    private fun almostEqual(a: Float, b: Float): Boolean {
        return (a - b).absoluteValue < 0.0001f
    }

}