package ru.casperix.collection

import ru.casperix.math.collection.LRUCache
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue
import kotlin.time.measureTime

class LRUCacheTest {
    @Test
    fun test() {
        val cache = LRUCache<String, Int>(3)

        cache.put("one", 1)
        cache.put("two", 2)
        cache.put("three", 3)

        assertEquals(1, cache.get("one"))

        cache.put("four", 4)

        assertEquals(null, cache.get("two"))
        assertEquals(3, cache.get("three"))
    }

    @Test
    fun performanceLRU() {
        val capacity = 1000_000
        val amounts = 2000_000

        val cache = LRUCache<Int, Int>(capacity)

        val putTime = measureTime {
            (0 until amounts).forEach {
                cache.put(it, it)
            }
        }
        val getTime = measureTime {
            (0 until amounts).forEach {
                val value = cache.get(it)
                assertTrue((value == null && it < capacity) || (value == it))
            }
        }

        println("LRU($capacity); put: ${putTime.inWholeMilliseconds}ms; get: ${getTime.inWholeMilliseconds}ms")
        assertTrue((putTime + getTime).inWholeMilliseconds < 1000)
    }

    @Test
    fun performanceHashMap() {
        val capacity = 1000_000

        val cache = mutableMapOf<Int, Int>()

        val putTime = measureTime {
            (0 until capacity).forEach {
                cache.put(it, it)
            }
        }
        val getTime = measureTime {
            (0 until capacity).forEach {
                val value = cache.get(it)
                assertEquals(value, it)
            }
        }

        println("HMAP($capacity); put: ${putTime.inWholeMilliseconds}ms; get: ${getTime.inWholeMilliseconds}ms")
        assertTrue((putTime + getTime).inWholeMilliseconds < 1000)
    }
}