package ru.casperix.math.mesh.component

import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.axis_aligned.float32.getAABBox
import ru.casperix.math.curve.float32.Circle2f
import ru.casperix.math.geometry.Polygon2f
import ru.casperix.math.intersection.float32.Intersection2Float
import ru.casperix.math.mesh.SpatialMap
import ru.casperix.math.mesh.float32.MeshPoint
import ru.casperix.math.straight_line.float32.LineSegment2f
import com.github.davidmoten.rtree2.Entries
import com.github.davidmoten.rtree2.RTree

class RTreePoint(initial: List<MeshPoint> = emptyList()) : SpatialMap<MeshPoint> {
    private var container = RTree.create(initial.map { Entries.entry(it, it.shape.toRPoint()) })

    override fun clear() {
        container = RTree.create()
    }

    override fun all(): List<MeshPoint> {
        return container.entries().map { it.value() }
    }

    override fun add(element: MeshPoint) {
        container = container.add(element, element.shape.toRPoint())
    }

    override fun remove(element: MeshPoint) {
        container = container.delete(element, element.shape.toRPoint())
    }

    override fun has(element: MeshPoint): Boolean {
        return container.search(element.shape.toRPoint()).firstOrNull() != null
    }

    override fun search(area: Box2f): Collection<MeshPoint> {
        return container.search(area.toRRectangle()).map { it.value() }
    }

    override fun search(area: Circle2f): Collection<MeshPoint> {
        return container.search(area.toRCircle()).map { it.value() }
    }

    override fun search(area: LineSegment2f): Collection<MeshPoint> {
        return container.search(area.toRLine()).map { it.value() }
    }

    override fun search(area: Polygon2f): Collection<MeshPoint> {
        return container.search(area.getAABBox().toRRectangle()) { p, r ->
            Intersection2Float.hasPointWithPolygon(p.toVector2f(), area)
        }.map { it.value() }
    }
}