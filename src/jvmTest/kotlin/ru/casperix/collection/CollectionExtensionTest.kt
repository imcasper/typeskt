package ru.casperix.collection

import ru.casperix.math.collection.getLooped
import kotlin.test.Test
import kotlin.test.assertEquals


class CollectionExtensionTest {
    @Test
    fun testLooped() {
        val smallList = listOf(1)

        assertEquals(1,        smallList.getLooped(-2))
        assertEquals(1,        smallList.getLooped(-1))
        assertEquals(1,        smallList.getLooped(0))
        assertEquals(1,        smallList.getLooped(1))
        assertEquals(1,        smallList.getLooped(2))

        val list = listOf(1, 2, 3)

        assertEquals(1,        list.getLooped(-6))
        assertEquals(2,        list.getLooped(-5))
        assertEquals(3,        list.getLooped(-4))
        assertEquals(1,        list.getLooped(-3))
        assertEquals(2,        list.getLooped(-2))
        assertEquals(3,        list.getLooped(-1))
        assertEquals(1,        list.getLooped(0))
        assertEquals(2,        list.getLooped(1))
        assertEquals(3,        list.getLooped(2))
        assertEquals(1,        list.getLooped(3))
        assertEquals(2,        list.getLooped(4))
        assertEquals(3,        list.getLooped(5))
        assertEquals(1,        list.getLooped(6))
        assertEquals(2,        list.getLooped(7))
        assertEquals(3,        list.getLooped(8))
    }
}