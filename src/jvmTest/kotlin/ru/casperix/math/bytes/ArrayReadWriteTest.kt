package ru.casperix.math.bytes

import ru.casperix.math.bytes.TemplateByteList.bytes
import ru.casperix.misc.DirectByteBuffer
import java.nio.ByteOrder
import kotlin.test.Test
import kotlin.test.assertEquals

class ArrayReadWriteTest {

    @Test
    fun testReader() {
        val bytes = ByteArray(bytes.size) { bytes[it] }

        listOf(ByteOrder.LITTLE_ENDIAN, ByteOrder.BIG_ENDIAN).forEach { javaByteOrder ->
            val javaBuffer = DirectByteBuffer(TemplateByteList.bytes.size).order(javaByteOrder)

            TemplateByteList.bytes.forEachIndexed { index, value ->
                javaBuffer.put(index, value)
            }


            val order = if (javaByteOrder == ByteOrder.LITTLE_ENDIAN) {
                ru.casperix.math.bytes.ByteOrder.LITTLE_ENDIAN
            } else {
                ru.casperix.math.bytes.ByteOrder.BIG_ENDIAN
            }
            val reader = ByteArrayReaderImpl(bytes, order)

            (0..bytes.size - 2).forEach {
                assertEquals(javaBuffer.getShort(it), reader.readShort(it))
            }

            (0..bytes.size - 4).forEach {
                assertEquals(javaBuffer.getInt(it), reader.readInt(it))
            }

            (0..bytes.size - 8).forEach {
                assertEquals(javaBuffer.getLong(it), reader.readLong(it))
            }
        }

    }

    @Test
    fun testWriter() {
        val sourceBytes = ByteArray(bytes.size) { bytes[it] }

        listOf(ByteOrder.LITTLE_ENDIAN, ByteOrder.BIG_ENDIAN).forEach { javaByteOrder ->


            val order = if (javaByteOrder == ByteOrder.LITTLE_ENDIAN) {
                ru.casperix.math.bytes.ByteOrder.LITTLE_ENDIAN
            } else {
                ru.casperix.math.bytes.ByteOrder.BIG_ENDIAN
            }
            val sourceReader = ByteArrayReaderImpl(sourceBytes, order)

            (0..sourceBytes.size - 2).forEach {
                val targetBytes = ByteArray(bytes.size)
                val targetReader = ByteArrayReaderImpl(targetBytes, order)
                val writer = ByteArrayWriterImpl(targetBytes, order)

                val last = sourceReader.readShort(it)
                writer.writeShort(it, last)

                val next = targetReader.readShort(it)
                assertEquals(last, next)
            }

            (0..sourceBytes.size - 4).forEach {
                val targetBytes = ByteArray(bytes.size)
                val targetReader = ByteArrayReaderImpl(targetBytes, order)
                val writer = ByteArrayWriterImpl(targetBytes, order)

                val last = sourceReader.readInt(it)
                writer.writeInt(it, last)

                val next = targetReader.readInt(it)
                assertEquals(last, next)
            }

            (0..sourceBytes.size - 8).forEach {
                val targetBytes = ByteArray(bytes.size)
                val targetReader = ByteArrayReaderImpl(targetBytes, order)
                val writer = ByteArrayWriterImpl(targetBytes, order)

                val last = sourceReader.readLong(it)
                writer.writeLong(it, last)

                val next = targetReader.readLong(it)
                assertEquals(last, next)
            }

        }

    }
}