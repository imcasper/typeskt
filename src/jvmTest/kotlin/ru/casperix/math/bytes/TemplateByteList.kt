package ru.casperix.math.bytes

object TemplateByteList {

    val bytes = listOf<Byte>(
        -128, 0, 0, 0,
        0, 0, 0, -128,
        127, -1, -1, -1,
        -1, -1, -1, 127,
        -1, -1, -1, -1,
        -128, -128, -128, -128,
        -1, -1, -1, -1,
        0, 0, 0, 0,
        127, 127, 127, 127,
    )
}