package ru.casperix.math.bytes

class ByteArrayReaderImpl(val bytes: ByteArray, override val order: ByteOrder) : ByteArrayReader {

    override fun readByte(offset: Int): Byte {
        return bytes[offset]
    }

    override fun readShort(offset: Int): Short {
        return if (order == ByteOrder.BIG_ENDIAN) {
            ((maskedInt(offset + 1)) or
                    (maskedInt(offset + 0) shl 8)).toShort()
        } else {
            ((maskedInt(offset + 0)) or
                    (maskedInt(offset + 1) shl 8)).toShort()
        }
    }

    override fun readInt(offset: Int): Int {
        return if (order == ByteOrder.BIG_ENDIAN) {
            maskedInt(offset + 3) or
                    (maskedInt(offset + 2) shl 8) or
                    (maskedInt(offset + 1) shl 16) or
                    (maskedInt(offset + 0) shl 24)
        } else {
            maskedInt(offset + 0) or
                    (maskedInt(offset + 1) shl 8) or
                    (maskedInt(offset + 2) shl 16) or
                    (maskedInt(offset + 3) shl 24)
        }
    }

    override fun readLong(offset: Int): Long {
        return if (order == ByteOrder.BIG_ENDIAN) {
            (maskedLong(offset + 7)) or
                    (maskedLong(offset + 6) shl 8) or
                    (maskedLong(offset + 5) shl 16) or
                    (maskedLong(offset + 4) shl 24) or
                    (maskedLong(offset + 3) shl 32) or
                    (maskedLong(offset + 2) shl 40) or
                    (maskedLong(offset + 1) shl 48) or
                    (maskedLong(offset + 0) shl 56)
        } else {
            (maskedLong(offset + 0)) or
                    (maskedLong(offset + 1) shl 8) or
                    (maskedLong(offset + 2) shl 16) or
                    (maskedLong(offset + 3) shl 24) or
                    (maskedLong(offset + 4) shl 32) or
                    (maskedLong(offset + 5) shl 40) or
                    (maskedLong(offset + 6) shl 48) or
                    (maskedLong(offset + 7) shl 56)
        }
    }

    override fun readFloat(offset: Int): Float {
        return Float.fromBits(readInt(offset))
    }

    override fun readDouble(offset: Int): Double {
        return Double.fromBits(readLong(offset))
    }

    private fun maskedInt(offset: Int): Int {
        return bytes[offset].toInt() and 0xFF
    }

    private fun maskedLong(offset: Int): Long {
        return bytes[offset].toLong() and 0xFFL
    }

}

