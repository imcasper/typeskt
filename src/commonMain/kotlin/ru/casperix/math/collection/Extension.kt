package ru.casperix.math.collection



fun <T> List<T>.getLooped(index: Int): T {
    return this[index.mod(size)]
}
