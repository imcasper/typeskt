package ru.casperix.math.collection

import ru.casperix.misc.compute

class LRUCache<K, V>(private val capacity: Int, val autoRemoveOldest:Boolean = true) {
    private val map = mutableMapOf<K, DoublyLinkedList.Node<Entry<K, V>>>()
    private var linkedList = DoublyLinkedList<Entry<K, V>>()

    class Entry<K, V>(val key: K, val value: V)

    init {
        require(capacity > 0) { "Capacity must be greater than zero." }
    }

    fun get(key: K): V? = map[key]?.run {
        linkedList.moveFront(this)
        data.value
    }

    fun removeOldest():V? {
        if (map.size > capacity) {
            val last = linkedList.removeLast()
            map.remove(last.data.key)
            return last.data.value
        }
        return null
    }

    fun clear() {
        map.clear()
        linkedList = DoublyLinkedList()
    }

    fun put(key: K, value: V) {
        map.compute(key) { _, lastEntry ->
            if (lastEntry != null) {
                linkedList.remove(lastEntry)
            }
            linkedList.pushFront(Entry(key, value))
        }

        if (autoRemoveOldest) {
            removeOldest()
        }
    }

    fun getOrPut(key: K, builder: () -> V): V {
        return map.getOrPut(key) {
            val value = builder()
            linkedList.pushFront(Entry(key, value))
        }.data.value.apply {
            if (autoRemoveOldest) {
                removeOldest()
            }
        }
    }

}