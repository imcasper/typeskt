package ru.casperix.math.complex.float32

import ru.casperix.math.complex.float64.ComplexValueDouble
import kotlin.math.pow
import kotlin.math.sqrt

class ComplexValueFloat(val a: Float, val b: Float) {

	operator fun plus(other: ComplexValueFloat): ComplexValueFloat {
		return ComplexValueFloat(this.a + other.a, this.b + other.b)
	}

	operator fun minus(other: ComplexValueFloat): ComplexValueFloat {
		return ComplexValueFloat(this.a - other.a, this.b - other.b)
	}

	operator fun times(value: Float): ComplexValueFloat {
		return ComplexValueFloat(this.a * value, this.b * value)
	}

	operator fun times(other: ComplexValueFloat): ComplexValueFloat {
		return ComplexValueFloat(this.a * other.a - this.b * other.b, this.a * other.b + this.b * other.a)
	}

	fun conjugate(): ComplexValueFloat {
		return ComplexValueFloat(a, -b)
	}

	fun norm(): Float {
		return sqrt(a * a + b * b)
	}

	operator fun div(other: ComplexValueFloat): ComplexValueFloat {
		val d = other.a.pow(2) + other.b.pow(2)
		return ComplexValueFloat((this.a * other.a + this.b * other.b) / d, (this.b * other.a - this.a * other.b) / d)
	}

	fun toComplexValued(): ComplexValueDouble {
		return ComplexValueDouble(a.toDouble(), b.toDouble())
	}
}