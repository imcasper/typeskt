package ru.casperix.math.quad_matrix

import ru.casperix.math.quad_matrix.float32.Matrix3f
import ru.casperix.math.quad_matrix.float32.Matrix4f
import ru.casperix.math.quad_matrix.float64.Matrix3d
import ru.casperix.math.quad_matrix.float64.Matrix4d

interface QuadMatrix<Self, Component : Number> {
    operator fun times(other: Self): Self

    fun transpose(): Self

    fun determinant(): Component

    fun inverse(): Self
}

fun matrix3x3(vararg float: Float): Matrix3f {
    return Matrix3f(floatArrayOf(*float))
}

fun matrix3x3(vararg double: Double): Matrix3d {
    return Matrix3d(doubleArrayOf(*double))
}

fun matrix4x4(vararg float: Float): Matrix4f {
    return Matrix4f(floatArrayOf(*float))
}

fun matrix4x4(vararg double: Double): Matrix4d {
    return Matrix4d(doubleArrayOf(*double))
}

//fun matrix3x3(floats: FloatArray): Matrix3f {
//    return Matrix3f(floats)
//}
//
//fun matrix3x3(doubles: DoubleArray): Matrix3d {
//    return Matrix3d(doubles)
//}
//
//fun matrix4x4(floats: FloatArray): Matrix4f {
//    return Matrix4f(floats)
//}
//
//fun matrix4x4(doubles: DoubleArray): Matrix4d {
//    return Matrix4d(doubles)
//}
