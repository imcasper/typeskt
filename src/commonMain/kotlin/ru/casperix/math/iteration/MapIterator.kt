package ru.casperix.math.iteration

fun <T> Iterator<T>.firstOrNull(condition: (T) -> Boolean): T? {
	forEach {
		if (condition(it)) return it
	}
	return null
}

fun <T> Iterator<T>.firstOrNull(): T? {
	forEach {
		return it
	}
	return null
}

fun <T> MutableIterator<T>.removeIf(action: (T) -> Boolean) {
	forEach {
		if (action(it)) {
			remove()
		}
	}
}

fun <Source, Target> Iterator<Source>.map(converter: (Source) -> Target?): Iterator<Target> {
	return MapIterator(this, converter)
}

class MapIterator<Source, Target>(val sourceItems: Iterator<Source>, val map: (source: Source) -> Target?) : Iterator<Target> {
	private var current: Target? = null

	init {
		current = getNext()
	}

	private fun getNext(): Target? {
		while (sourceItems.hasNext()) {
			val pos = sourceItems.next()
			map(pos)?.let {
				return it
			}
		}
		return null
	}

	override fun next(): Target {
		val last = current ?: throw Error("Invalid next. You must check hasNext first")
		current = getNext()
		return last
	}

	override fun hasNext(): Boolean {
		return current != null
	}

}