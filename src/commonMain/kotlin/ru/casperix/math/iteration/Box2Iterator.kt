package ru.casperix.math.iteration


import ru.casperix.math.axis_aligned.int32.Box2i
import ru.casperix.math.vector.int32.Vector2i

class Box2Iterator(val source: Box2i) : Iterator<Vector2i> {
	private var x: Int
	private var y: Int
	private var next: Vector2i?

	init {
		x = source.min.x
		y = source.min.y
		next = Vector2i(x, y)
	}

	/**
	 * Returns the next element in the iteration.
	 */
	override operator fun next(): Vector2i {
		val result = next!!

		if (++x > source.max.x) {
			x = source.min.x
			if (++y > source.max.y) {
				y = source.min.y
					next = null
					return result
			}
		}
		next = Vector2i(x, y)
		return result
	}


	/**
	 * Returns `true` if the iteration has more elements.
	 */
	override operator fun hasNext(): Boolean {
		return next != null
	}
}