package ru.casperix.math.spherical.float64

import ru.casperix.math.interpolation.float64.InterpolateDoubleFunction
import ru.casperix.math.interpolation.float64.linearInterpolate
import ru.casperix.math.spherical.float32.SphericalCoordinateFloat
import ru.casperix.math.vector.float64.Vector3d
import kotlinx.serialization.Serializable
import kotlin.math.cos
import kotlin.math.sin

/**
 * 	@see https://en.wikipedia.org/wiki/Spherical_coordinate_system
 *
 * 	@param range -- dest to zero
 * 	@param verticalAngle -- angle from Z axis
 * 	@param horizontalAngle -- angle  from X axis
 */
@Serializable
data class SphericalCoordinateDouble(val range: Double, val verticalAngle: Double, val horizontalAngle: Double) {
	fun fromSpherical(): Vector3d {

		val sinV = sin(verticalAngle)
		val cosV = cos(verticalAngle)
		val cosH = cos(horizontalAngle)
		val sinH = sin(horizontalAngle)
		return Vector3d(range * sinV * cosH, range * sinV * sinH, range * cosV)
	}

	fun asVector3d(): Vector3d {
		return Vector3d(range, verticalAngle, horizontalAngle)
	}

	fun toSphericalCoordinatef(): SphericalCoordinateFloat {
		return SphericalCoordinateFloat(range.toFloat(), verticalAngle.toFloat(), horizontalAngle.toFloat())
	}

	fun interpolate(A: SphericalCoordinateDouble, B: SphericalCoordinateDouble, factor: Double, interpolator: InterpolateDoubleFunction = linearInterpolate): SphericalCoordinateDouble {
		return SphericalCoordinateDouble(
			interpolator(A.range, B.range, factor),
			interpolator(A.verticalAngle, B.verticalAngle, factor),
			interpolator(A.horizontalAngle, B.horizontalAngle, factor)
		)
	}

	companion object {
		val ZERO = SphericalCoordinateDouble(0.0, 0.0, 0.0)
	}
}