package ru.casperix.math.curve

import ru.casperix.math.curve.float64.Curve2d
import ru.casperix.math.curve.float32.ParametricCurve2f
import ru.casperix.misc.toPrecision

object CurveHelper {
    fun get(curve: ParametricCurve2f, steps: Int): List<Float> {
        val points = (0..steps).map {
            curve.getPosition(it.toFloat() / steps.toFloat())
        }

        val dests = (0 until steps).map {
            val a = points[it]
            val b = points[it + 1]
            a.distTo(b)
        }

        return dests
    }

    fun getRough(curve: ParametricCurve2f, steps: Int): Float {
        val dests = get(curve, steps)
        val rough = dests.max() / dests.min()
        return rough
    }

    fun get(curve: Curve2d, steps: Int): List<Double> {
        val points = (0..steps).map {
            curve.getPosition(it.toDouble() / steps.toDouble())
        }

        val dests = (0 until steps).map {
            val a = points[it]
            val b = points[it + 1]
            a.distTo(b)
        }

        return dests
    }

    fun getRough(curve: Curve2d, steps: Int): Double {
        val dests = get(curve, steps)
        val rough = dests.max() / dests.min()
        return rough
    }

    fun getAndPrint(curve: ParametricCurve2f, steps: Int): String {

        return "rough:${getRough(curve, steps).toPrecision(2)}"
    }

    fun calculateLength(curve: ParametricCurve2f, steps:Int): Float {
        val points = (0..steps).map {
            curve.getPosition(it / steps.toFloat())
        }
        var dest = 0f

        (0 until steps).forEach {
            dest += points[it].distTo(points[it + 1])
        }
        return dest
    }

    fun calculateLength(curve: Curve2d, steps:Int): Double {
        val points = (0..steps).map {
            curve.getPosition(it / steps.toDouble())
        }
        var dest = 0.0

        (0 until steps).forEach {
            dest += points[it].distTo(points[it + 1])
        }
        return dest
    }
}