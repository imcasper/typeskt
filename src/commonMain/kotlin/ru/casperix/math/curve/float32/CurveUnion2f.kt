package ru.casperix.math.curve.float32

import ru.casperix.math.vector.float32.Vector2f
import kotlinx.serialization.Serializable


@Serializable
class CurveUnion2f(val first: ParametricCurve2f, val second: ParametricCurve2f) : ParametricCurve2f {
    private val firstLength = first.length()
    private val secondLength = second.length()
    private val length = firstLength + secondLength

    override fun length(): Float {
        return length
    }

    override fun invert(): CurveUnion2f {
        return CurveUnion2f(second.invert(), first.invert())
    }

    private fun getPart(t: Float): Pair<ParametricCurve2f, Float> {
        val position = t * length
        return if (position <= firstLength) {
            Pair(first, position / firstLength)
        } else {
            Pair(second, (position - firstLength) / secondLength)
        }

    }

    override fun divide(t: Float): Pair<ParametricCurve2f, ParametricCurve2f> {
        val (curve, s) = getPart(t)
        val (left, right) = curve.divide(s)
        return if (curve == first) {
            Pair(left, right + second)
        } else {
            Pair(first + left, right)
        }
    }

    override fun getPosition(t: Float): Vector2f {
        val (curve, s) = getPart(t)
        return curve.getPosition(s)
    }

    override fun getTangent(t: Float): Vector2f {
        val (curve, s) = getPart(t)
        return curve.getTangent(s)
    }

    override fun getNormal(t: Float): Vector2f {
        val (curve, s) = getPart(t)
        return curve.getNormal(s)
    }
}