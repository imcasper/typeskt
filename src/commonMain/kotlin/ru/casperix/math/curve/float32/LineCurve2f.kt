package ru.casperix.math.curve.float32

import ru.casperix.math.geometry.Line2f
import ru.casperix.math.geometry.delta
import ru.casperix.math.interpolation.float32.InterpolationFloat
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.math.vector.rotateCCW
import kotlinx.serialization.Serializable

@Deprecated(message = "Use LineSegment2f")
@Serializable
data class LineCurve2f(val line: Line2f) : ParametricCurve2f {
    override val start: Vector2f get() = line.v0
    override val finish: Vector2f get() = line.v1

    constructor(start: Vector2f, finish: Vector2f) : this(Line2f(start, finish))

    override fun getPosition(t: Float): Vector2f {
        return InterpolationFloat.vector2(start, finish, t)
    }

    override fun invert(): LineCurve2f {
        return LineCurve2f(finish, start)
    }

    override fun getTangent(t: Float): Vector2f {
        return line.delta().normalize()
    }

    override fun getNormal(t: Float): Vector2f {
        return getTangent(t).rotateCCW()
    }

    override fun length(): Float {
        return line.v0.distTo(line.v1)
    }

    override fun divide(t: Float): Pair<LineCurve2f, LineCurve2f> {
        val d = line.delta()
        return Pair(LineCurve2f(start, start + d * t), LineCurve2f(start + d * t, finish))

    }

    override fun getProjection(point: Vector2f): Float {
        val direction = line.delta().normalize()
        val projection = (point - start).dot(direction)
        return projection / length()
    }

    override fun grow(startOffset: Float, finishOffset: Float): LineCurve2f {
        val offset = line.delta().normalize()
        return LineCurve2f(start - offset * startOffset, finish + offset * finishOffset)
    }
}