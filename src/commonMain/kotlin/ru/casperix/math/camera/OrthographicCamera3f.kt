package ru.casperix.math.camera

import ru.casperix.math.axis_aligned.float32.Dimension2f
import ru.casperix.math.quad_matrix.float32.Matrix4f
import ru.casperix.math.vector.float32.Vector2f

class OrthographicCamera3f(val state: OrthographicCameraStateOld) {
    val zoom get() = state.zoom
    val position get() = state.position

    var transform: CameraTransform3f = CameraTransform3f(Matrix4f.IDENTITY, Matrix4f.IDENTITY, Dimension2f.ZERO, 0f, 1000f)

    fun update(viewport: Dimension2f) {
        transform = calculate(viewport, transform.near, transform.far, state.zoom, state.position)
    }

    companion object {
        fun calculate(
            viewport: Dimension2f,
            near: Float,
            far: Float,
            zoom: Float,
            position: Vector2f
        ): CameraTransform3f {
            val projection = Matrix4f.orthographic(
                -viewport.width / 2f / zoom,
                viewport.width / 2f / zoom,
                -viewport.height / 2f / zoom,
                viewport.height / 2f / zoom,
                near,
                far
            )

            val view = Matrix4f.translate(-position.expand(0f))
            return CameraTransform3f(projection, view, viewport, near, far)
        }
    }
}