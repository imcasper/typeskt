package ru.casperix.math.color.rgb

import kotlinx.serialization.Serializable
import ru.casperix.math.color.misc.ColorDecoder.doubleToUByte

@Serializable
data class RgbColor3d(val red: Double, val green: Double, val blue: Double) : RgbColor {
    constructor(component: Double) : this(component, component, component)

    override fun toColor3b() = RgbColor3b(doubleToUByte(red), doubleToUByte(green), doubleToUByte(blue))
    override fun toColor3d() = this
    override fun toColor3f() = RgbColor3f(red.toFloat(), green.toFloat(), blue.toFloat())

}

