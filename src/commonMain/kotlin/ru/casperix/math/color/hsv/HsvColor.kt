package ru.casperix.math.color.hsv

import ru.casperix.math.color.PhysicColor
import ru.casperix.math.vector.float32.Vector3f

interface HsvColor : PhysicColor {
    fun toVector3f(): Vector3f
}