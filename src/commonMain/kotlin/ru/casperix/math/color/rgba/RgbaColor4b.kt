package ru.casperix.math.color.rgba

import kotlinx.serialization.Serializable
import ru.casperix.math.color.misc.ColorDecoder.uByteToDouble
import ru.casperix.math.color.misc.ColorDecoder.uByteToFloat

@Serializable
data class RgbaColor4b(val red: UByte, val green: UByte, val blue: UByte, val alpha: UByte) : RgbaColor {
    override fun toColor4b() = this
    override fun toColor4d()= RgbaColor4d(uByteToDouble(red), uByteToDouble(green), uByteToDouble(blue), uByteToDouble(alpha))
    override fun toColor4f()= RgbaColor4f(uByteToFloat(red), uByteToFloat(green), uByteToFloat(blue), uByteToFloat(alpha))


}