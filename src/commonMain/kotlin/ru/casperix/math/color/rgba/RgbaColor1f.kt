package ru.casperix.math.color.rgba

import kotlinx.serialization.Serializable

@Serializable
data class RgbaColor1f(val value: Float) : RgbaColor {
    override fun toColor4b() = toColor1i().toColor4b()
    override fun toColor4d() = toColor1i().toColor4d()
    override fun toColor4f() = toColor1i().toColor4f()

    override fun toColor1i(): RgbaColor1i {
        return RgbaColor1i(value.toRawBits().toUInt())
    }

    override fun toColor1f() = this
}