package ru.casperix.math.color.rgba

import kotlinx.serialization.Serializable
import ru.casperix.math.color.misc.ColorDecoder.doubleToUByte

@Serializable
data class RgbaColor4d(val red: Double, val green: Double, val blue: Double, val alpha: Double) : RgbaColor {
    constructor(component: Double) : this(component, component, component, component)

    override fun toColor4b() = RgbaColor4b(doubleToUByte(red), doubleToUByte(green), doubleToUByte(blue), doubleToUByte(alpha))
    override fun toColor4d() = this
    override fun toColor4f() = RgbaColor4f(red.toFloat(), green.toFloat(), blue.toFloat(), alpha.toFloat())
}

