package ru.casperix.math.color.rgba

import ru.casperix.math.vector.float32.Vector4f
import ru.casperix.math.vector.float64.Vector4d

fun Vector4d.toRGBAColor(): RgbaColor4d {
    return RgbaColor4d(x, y, z, w)
}

fun Vector4f.toRGBAColor(): RgbaColor4f {
    return RgbaColor4f(x, y, z, w)
}

