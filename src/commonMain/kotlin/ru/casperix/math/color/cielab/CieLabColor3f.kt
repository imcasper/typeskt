package ru.casperix.math.color.cielab

import kotlinx.serialization.Serializable
import ru.casperix.math.color.hsv.HsvColor3f
import ru.casperix.math.color.rgb.RgbColor3f
import ru.casperix.math.color.rgba.RgbaColor4f

@Deprecated(message = "Not yet implemented")
@Serializable
data class CieLabColor3f(val lightness: Float, val a: Float, val b: Float) : CieLabColor {
    constructor(component: Float) : this(component, component, component)

    override fun toRGB(): RgbColor3f {
        TODO("Not yet implemented")
    }

    override fun toRGBA(): RgbaColor4f {
        TODO("Not yet implemented")
    }

    override fun toHSV(): HsvColor3f {
        TODO("Not yet implemented")
    }


}
