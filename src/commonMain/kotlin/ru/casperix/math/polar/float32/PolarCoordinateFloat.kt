package ru.casperix.math.polar.float32

import ru.casperix.math.angle.float32.RadianFloat
import ru.casperix.math.geometry.fDEGREE_TO_RADIAN
import ru.casperix.math.vector.float32.Vector2f
import kotlinx.serialization.Serializable
import kotlin.math.cos
import kotlin.math.sin

@Serializable
data class PolarCoordinateFloat(val range: Float, val angle: RadianFloat) {
//    constructor(range: Float, angle: DegreeFloat) : this(range, angle.toRadian())


    fun toDecart(): Vector2f {
        return Vector2f(range * cos(angle.value), range * sin(angle.value))
    }

    companion object {
        fun byRadian(range: Float, radian: Float): PolarCoordinateFloat {
            return PolarCoordinateFloat(range, RadianFloat(radian))
        }

        fun byDegree(range: Float, degree: Float): PolarCoordinateFloat {
            return PolarCoordinateFloat(range, RadianFloat(degree * fDEGREE_TO_RADIAN))
        }
    }
}

