package ru.casperix.math.test

import ru.casperix.math.angle.float32.DegreeFloat
import ru.casperix.math.angle.float32.RadianFloat
import ru.casperix.math.collection.getLooped
import ru.casperix.math.geometry.Line2f
import ru.casperix.math.quad_matrix.float32.Matrix3f
import ru.casperix.math.quad_matrix.float32.Matrix4f
import ru.casperix.math.straight_line.float32.LineSegment2f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.math.vector.float32.Vector3f
import ru.casperix.math.vector.float32.Vector4f
import ru.casperix.misc.mapping
import kotlin.math.absoluteValue

object FloatCompare {
    var defaultError = 0.0001f

    fun isLike(a: Float, b: Float, error:Float = defaultError): Boolean {
        if (!a.isFinite() || !b.isFinite()) {
            return a == b
        }
        return (a - b).absoluteValue < error
    }

    fun isLike(a: Vector2f, b: Vector2f, error:Float = defaultError): Boolean {
        return isLike(a.x, b.x, error) && isLike(a.y, b.y, error)
    }

    fun isLike(a: Line2f, b: Line2f, error:Float = defaultError): Boolean {
        return isLike(a.v0, b.v0, error) && isLike(a.v1, b.v1, error)
    }

    fun isLike(a: LineSegment2f, b: LineSegment2f, error:Float = defaultError): Boolean {
        return isLike(a.start, b.start, error) && isLike(a.finish, b.finish, error)
    }

    fun isLike(a: Vector3f, b: Vector3f, error:Float = defaultError): Boolean {
        return isLike(a.x, b.x, error) && isLike(a.y, b.y, error) && isLike(a.z, b.z, error)
    }

    fun isLike(a: Vector4f, b: Vector4f, error:Float = defaultError): Boolean {
        return isLike(a.x, b.x, error) && isLike(a.y, b.y, error) && isLike(a.z, b.z, error) && isLike(a.w, b.w, error)
    }

    fun isLike(a: ru.casperix.math.angle.float32.DegreeFloat, b: ru.casperix.math.angle.float32.DegreeFloat, error:Float = defaultError): Boolean {
        return isLike(a.value, b.value, error)
    }

    fun isLike(a: RadianFloat, b: RadianFloat, error:Float = defaultError): Boolean {
        return isLike(a.value, b.value, error)
    }

    fun isLike(a: Matrix3f, b: Matrix3f, error:Float = defaultError): Boolean {
        a.data.indices.forEach {
            if (!isLike(a[it], b[it], error)) return false
        }
        return true
    }

    fun isLike(a: Matrix4f, b: Matrix4f, error:Float = defaultError): Boolean {
        a.data.indices.forEach {
            if (!isLike(a[it], b[it], error)) return false
        }
        return true
    }


    /**
     * Result contains only non-like points
     */
    fun Collection<Vector2f>.unique(error:Float = defaultError): Collection<Vector2f> {
        return mapping {
            Pair((it / error).roundToVector2i(), it)
        }.values
    }

    /**
     *  Remove like if neighbours:
     *
     *  0, 1
     *  2, 4
     *  1, 3
     *  1, 3    -   remove
     *  1, 3    -   remove
     *  0, 1
     *  1, 1
     *
     */
    fun List<Vector2f>.uniqueOrdered(error:Float = defaultError): List<Vector2f> {
        return mapIndexedNotNull { index, current ->
            val last = getLooped(index - 1)
            if (isLike(last, current, error)) {
                null
            } else {
                current
            }
        }
    }

}