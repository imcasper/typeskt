package ru.casperix.math.interpolation.float64

import kotlin.math.*

typealias InterpolateTypeFunctiond<Type> = (Type, Type, Double) -> Type
typealias InterpolateDoubleFunction = (Double, Double, Double) -> Double

/**
 * 	x= [0 ; 1]
 *
 * 	x = 0.0
 * 	value  grow from a to b
 * 	x = 0.5
 * 	value low from b to a
 * 	x = 1.0
 */
val parabolaABAInterpolate: InterpolateDoubleFunction = { a: Double, b: Double, x: Double ->
	val v = if (x < 0.5) {
		x
	} else {
		1.0 - x
	}
	a + b * sqrt(v * 2.0)
}


val linearInterpolate: InterpolateDoubleFunction = { a: Double, b: Double, x: Double ->
	a * (1.0 - x) + b * x
}

val cosineInterpolate: InterpolateDoubleFunction = { a: Double, b: Double, x: Double ->
	val ft = x * PI
	val f = (1.0 - cos(ft)) * 0.5
	a * (1.0 - f) + b * f
}

val hermiteInterpolate: InterpolateDoubleFunction = { a: Double, b: Double, x: Double ->
	a + (b - a) * (3.0 - 2.0 * x) * x * x
}

/**
 * @source https://spicyyoghurt.com/tools/easing-functions
 */

val easeInQuad: InterpolateDoubleFunction = { a: Double, b: Double, t: Double ->
	val c = b - a
	c * t * t + a
}

val easeOutQuad: InterpolateDoubleFunction = { a: Double, b: Double, t: Double ->
	val c = b - a
	-c * t * (t - 2) + a
}

val easeInOutQuad: InterpolateDoubleFunction = { a: Double, b: Double, t: Double ->
	val c = b - a
	val dt = t * 2.0

	if (dt < 1.0) {
		c / 2 * dt * dt + a
	} else {
		-c / 2 * ((dt - 1.0) * (dt - 3.0) - 1.0) + a
	}
}

val easeInCubic: InterpolateDoubleFunction = { a: Double, b: Double, t: Double ->
	val c = b - a
	c * t * t * t + a
}

val easeOutCubic: InterpolateDoubleFunction = { a: Double, b: Double, t: Double ->
	val c = b - a
	val dt = t - 1.0
	c * (dt * dt * dt + 1) + a
}

val easeInOutCubic: InterpolateDoubleFunction = { a: Double, b: Double, t: Double ->
	val t1 = t * 2.0
	val c = b - a

	if (t1 < 1.0) {
		c / 2 * t1 * t1 * t1 + a
	} else {
		val t2 = t1 - 2.0
		c / 2 * (t2 * t2 * t2 + 2) + a
	}
}

val easeInQuartic: InterpolateDoubleFunction = { a: Double, b: Double, t: Double ->
	val c = b - a
	c * t * t * t * t + a
}

val easeOutQuartic: InterpolateDoubleFunction = { a: Double, b: Double, t: Double ->
	val c = b - a
	val dt = t - 1.0
	c * (dt * dt * dt * dt - 1) + a
}

val easeInOutQuartic: InterpolateDoubleFunction = { a: Double, b: Double, t: Double ->
	val t1 = t * 2.0
	val c = b - a

	if (t1 < 1.0) {
		c / 2 * t1 * t1 * t1 * t1 + a
	} else {
		val t2 = t1 - 2.0
		c / 2 * (t2 * t2 * t2 * t2 - 2) + a
	}
}

val easeInQuintic: InterpolateDoubleFunction = { a: Double, b: Double, t: Double ->
	val c = b - a
	c * t * t * t * t * t + a
}

val easeOutQuintic: InterpolateDoubleFunction = { a: Double, b: Double, t: Double ->
	val c = b - a
	val dt = t - 1.0
	c * (dt * dt * dt * dt * dt + 1) + a
}

val easeInOutQuintic: InterpolateDoubleFunction = { a: Double, b: Double, t: Double ->
	val t1 = t * 2.0
	val c = b - a

	if (t1 < 1.0) {
		c / 2 * t1 * t1 * t1 * t1 * t1 + a
	} else {
		val t2 = t1 - 2.0
		c / 2 * (t2 * t2 * t2 * t2 * t2 + 2) + a
	}
}

val easeInOutElastic: InterpolateDoubleFunction = { b:Double, a1:Double, t:Double->
	val c = a1 - b

	val s: Double
	val p = 0.3 * 1.5
	var a = c
	if (t == 0.0) {
		b
	} else {
		val t1 = t * 2.0
		if (t1 == 2.0) {
			b + c
		} else {

			if (a < c.absoluteValue) {
				a = c
				s = p / 4
			} else {
				s = p / (2 * PI) * asin(c / a)
			}
			val t2 = t1 - 1.0
			if (t1 < 1) {
				-.5 * (a * 2.0.pow(10 * t2) * sin((t2 - s) * (2 * PI) / p)) + b
			}else {
				a * 2.0.pow( -10 * t2) * sin((t2 - s) * (2 * PI) / p) * .5 + c + b;
			}
		}
	}

}