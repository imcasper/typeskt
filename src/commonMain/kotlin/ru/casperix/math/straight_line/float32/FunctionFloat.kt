package ru.casperix.math.straight_line.float32

import ru.casperix.math.straight_line.AbstractFunction


fun interface FunctionFloat : AbstractFunction<Float>