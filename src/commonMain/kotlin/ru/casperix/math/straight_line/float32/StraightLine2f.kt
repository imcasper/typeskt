package ru.casperix.math.straight_line.float32

import ru.casperix.math.straight_line.StraightLine
import ru.casperix.math.vector.float32.Vector2f

/**
 *  Alternative for class [casperix.math.geometry.Line]
 */
class StraightLine2f(override val b: Float, override val m: Float) : StraightLine<Vector2f, Float>, FunctionFloat {
    override fun get(x: Float): Float {
        return b + m * x
    }

    companion object {
        /**
         * x/a+y/b=1
         * y = b+(-b/a)*x
         */
        fun byIntercept(a: Float, b: Float): StraightLine2f {
            return StraightLine2f(b, -b / a)
        }

        /**
         *  y=b+mx
         */
        fun byInterceptAndSlope(b: Float, m: Float): StraightLine2f {
            return StraightLine2f(b, m)
        }

        /**
         * (y-y1) / (x-x1) = (y2-y1) / (x2-x1)
         * y=y1 - (y2-y1)/(x2-x1) * x1 + (y2-y1)/(x2-x1) * x
         */
        fun byPoints(p1: Vector2f, p2: Vector2f): StraightLine2f {
            val (x1, y1) = p1
            val (x2, y2) = p2
            val m = (y2 - y1) / (x2 - x1)
            val b = y1 -  m * x1
            return StraightLine2f(b, m)
        }
    }
}