package ru.casperix.math.straight_line.float32

import ru.casperix.math.curve.float32.ParametricCurve2f
import ru.casperix.math.geometry.Line2f
import ru.casperix.math.interpolation.float32.InterpolationFloat
import ru.casperix.math.straight_line.LineSegment
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.math.vector.rotateCCW
import kotlinx.serialization.Serializable

/**
 *  Alternative for class [casperix.math.geometry.Line]
 */
@Serializable
data class LineSegment2f(override val start: Vector2f, override val finish: Vector2f) : LineSegment<Vector2f>, ParametricCurve2f {
    override fun getPosition(t: Float): Vector2f {
        return InterpolationFloat.vector2(start, finish, t)
    }

    override fun divide(t: Float): Pair<LineSegment2f, LineSegment2f> {
        val d = delta()
        return Pair(LineSegment2f(start, start + d * t), LineSegment2f(start + d * t, finish))

    }

    fun median(): Vector2f {
        return (start + finish) / 2f
    }

    fun delta(): Vector2f {
        return finish - start
    }

    override fun length(): Float {
        return delta().length()
    }

    override fun invert(): LineSegment2f {
        return LineSegment2f(finish, start)
    }

    override fun getTangent(t: Float): Vector2f {
        return direction()
    }

    override fun getNormal(t: Float): Vector2f {
        return normal()
    }

    fun direction(): Vector2f {
        return delta().normalize()
    }

    fun normal(): Vector2f {
        return direction().rotateCCW()
    }

    override fun getProjection(position: Vector2f): Float {
        val direction = delta().normalize()
        val projection = (position - start).dot(direction)
        return projection / length()
    }

    override fun grow(startOffset: Float, finishOffset: Float): LineSegment2f {
        val offset = delta().normalize()
        return LineSegment2f(start - offset * startOffset, finish + offset * finishOffset)
    }

    fun toLine(): Line2f {
        return Line2f(start, finish)
    }

    companion object {
        fun byDelta(pivot: Vector2f, delta: Vector2f): LineSegment2f {
            return LineSegment2f(pivot, pivot + delta)
        }
    }
}