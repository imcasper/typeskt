package ru.casperix.math.axis_aligned

import ru.casperix.math.axis_aligned.float32.Dimension2f
import ru.casperix.math.axis_aligned.float32.Dimension3f
import ru.casperix.math.axis_aligned.int32.Dimension2i
import ru.casperix.math.axis_aligned.int32.Dimension3i

interface Dimension2<Component:Number> {
    val width:Component
    val height:Component
}

interface Dimension3<Component:Number> {
    val width:Component
    val height:Component
    val depth:Component
}


fun dimensionOf(width: Int, height: Int): Dimension2i {
    return Dimension2i(width, height)
}

fun dimensionOf(width: Float, height: Float): Dimension2f {
    return Dimension2f(width, height)
}

fun dimensionOf(width: Int, height: Int, depth:Int): Dimension3i {
    return Dimension3i(width, height, depth)
}

fun dimensionOf(width: Float, height: Float, depth:Float): Dimension3f {
    return Dimension3f(width, height, depth)
}