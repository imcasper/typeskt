package ru.casperix.math.axis_aligned.float32

import ru.casperix.math.axis_aligned.int32.Axis3i
import ru.casperix.math.axis_aligned.Box
import ru.casperix.math.axis_aligned.float64.Box3d
import ru.casperix.math.geometry.Quad
import ru.casperix.math.vector.float32.Vector3f
import kotlinx.serialization.Serializable

/**
 *
 * 	AxisAlignedBox
 *        3/----/7
 * 	  1/----/5
 *
 * 		  2/----/6
 * Z		0/----/4
 * |
 * |   Y
 * /===X
 *
 *
 * */
@Serializable
data class Box3f(override val min: Vector3f, override val max: Vector3f) : Box<Vector3f, Float> {
	override val center: Vector3f get() = (max + min) * 0.5f
	override val dimension: Vector3f get() = max - min
	override val volume: Float get() = dimension.x * dimension.y * dimension.z

	init {
		if (!min.lessOrEq(max)) throw Error("$min > $max mst be valid for $Box3d");
	}

	override fun isInside(point: Vector3f): Boolean {
		return min.x <= point.x && point.x <= max.x && min.y <= point.y && point.y <= max.y && min.z <= point.z && point.z <= max.z
	}

	fun grow(value: Float): Box3f {
		return Box3f(min - Vector3f(value), max + Vector3f(value))
	}

	companion object {
		fun createOrNull(min: Vector3f, max: Vector3f): Box3f? {
			if (!min.lessOrEq(max)) return null
			return Box3f(min, max)
		}

		fun byCorners(A: Vector3f, B: Vector3f): Box3f {
			val min = A.lower(B)
			val max = A.upper(B)
			return Box3f(min, max)
		}

		fun byRadius(center: Vector3f, radius: Vector3f): Box3f {
			return Box3f(center - radius, center + radius)
		}

		fun byDimension(start: Vector3f, dimension: Vector3f): Box3f {
			return Box3f(start, start + dimension)
		}
	}

	val indices: IntRange get() = 0..7

	fun getCorner(index: Int): Vector3f {
		return when (index) {
			0 -> Vector3f(min.x, min.y, min.z)
			1 -> Vector3f(min.x, min.y, max.z)
			2 -> Vector3f(min.x, max.y, min.z)
			3 -> Vector3f(min.x, max.y, max.z)
			4 -> Vector3f(max.x, min.y, min.z)
			5 -> Vector3f(max.x, min.y, max.z)
			6 -> Vector3f(max.x, max.y, min.z)
			7 -> Vector3f(max.x, max.y, max.z)
			else -> throw Error("Invalid index")
		}
	}

	fun getSideByDirection(direction: Axis3i): Quad<Vector3f> {
		return when (direction) {
			Axis3i.NEGATIVE_X -> Quad(getCorner(0), getCorner(2), getCorner(3), getCorner(1))
			Axis3i.POSITIVE_X -> Quad(getCorner(4), getCorner(5), getCorner(7), getCorner(6))
			Axis3i.NEGATIVE_Y -> Quad(getCorner(0), getCorner(1), getCorner(5), getCorner(4))
			Axis3i.POSITIVE_Y -> Quad(getCorner(2), getCorner(6), getCorner(7), getCorner(3))
			Axis3i.NEGATIVE_Z -> Quad(getCorner(0), getCorner(4), getCorner(6), getCorner(2))
			Axis3i.POSITIVE_Z -> Quad(getCorner(1), getCorner(3), getCorner(7), getCorner(5))
		}
	}
}

