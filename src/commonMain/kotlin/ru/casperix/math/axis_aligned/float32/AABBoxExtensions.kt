package ru.casperix.math.axis_aligned.float32

import ru.casperix.math.geometry.Polygon2f
import ru.casperix.math.straight_line.float32.LineSegment2f


fun Polygon2f.getAABBox() : Box2f {
    val vertices = getVertices()
    return Box2f(
        vertices.reduce { a, b -> a.lower(b) },
        vertices.reduce { a, b -> a.upper(b) },
    )
}

fun LineSegment2f.getAABBox(): Box2f {
    return Box2f.byCorners(start, finish)
}