package ru.casperix.math.axis_aligned.float32

import ru.casperix.math.vector.float32.Vector3f


/**
 * 	Axis aligned direction
 */
enum class Axis3f(val index: Int, val value: Vector3f) {
	NEGATIVE_X(0, -Vector3f.X),
	POSITIVE_X(1, Vector3f.X),
	NEGATIVE_Y(2, -Vector3f.Y),
	POSITIVE_Y(3, Vector3f.Y),
	NEGATIVE_Z(4, -Vector3f.Z),
	POSITIVE_Z(5, Vector3f.Z);

	fun invert(): Axis3f {
		return when (this) {
			NEGATIVE_Z -> POSITIVE_Z
			POSITIVE_Z -> NEGATIVE_Z
			NEGATIVE_Y -> POSITIVE_Y
			POSITIVE_Y -> NEGATIVE_Y
			NEGATIVE_X -> POSITIVE_X
			POSITIVE_X -> NEGATIVE_X
		}
	}
}