package ru.casperix.math.axis_aligned.float32

import ru.casperix.math.axis_aligned.Dimension3
import ru.casperix.math.vector.float32.Vector3f
import ru.casperix.math.vector.int32.Vector3i
import kotlinx.serialization.Serializable
import kotlin.math.roundToInt

@Serializable
data class Dimension3f(override val width: Float, override val height: Float, override val depth:Float) : Dimension3<Float> {

    fun roundToVector3i(): Vector3i {
        return Vector3i(width.roundToInt(), height.roundToInt(), depth.roundToInt())
    }

    fun toVector3f(): Vector3f {
        return Vector3f(width, height, depth)
    }

    companion object {
        val ZERO = Dimension3f(0f, 0f, 0f)
    }
}