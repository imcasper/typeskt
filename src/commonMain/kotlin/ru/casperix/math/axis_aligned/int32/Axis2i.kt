package ru.casperix.math.axis_aligned.int32

import ru.casperix.math.vector.int32.Vector2i


/**
 * 	Axis aligned direction
 */
enum class Axis2i(val index: Int, val value: Vector2i) {
	NEGATIVE_X(0, -Vector2i.X),
	POSITIVE_X(1, Vector2i.X),
	NEGATIVE_Y(2, -Vector2i.Y),
	POSITIVE_Y(3, Vector2i.Y);

	fun invert(): Axis2i {
		return when (this) {
			NEGATIVE_Y -> POSITIVE_Y
			POSITIVE_Y -> NEGATIVE_Y
			NEGATIVE_X -> POSITIVE_X
			POSITIVE_X -> NEGATIVE_X
		}
	}
}