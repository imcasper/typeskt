package ru.casperix.math.geometry.builder

enum class BorderMode {
    CENTER,
    INSIDE,
    OUTSIDE,
}