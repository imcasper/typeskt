package ru.casperix.math.geometry

import ru.casperix.math.collection.getLooped
import kotlinx.serialization.Serializable

@Serializable
data class CustomPolygon<Point : Any>(val points: List<Point>) : Polygon<Point> {

    constructor(vararg points: Point) : this(points.toList())

    override fun getVertices(): List<Point> {
        return points
    }

    override fun <TargetPoint : Any> convert(converter: (Point) -> TargetPoint): CustomPolygon<TargetPoint> {
        val points = getVertices()
        return CustomPolygon(points.map(converter))
    }

    override fun getEdgeList(): List<Line<Point>> {
        return points.mapIndexed { index, point ->
            Line(point, points.getLooped(index + 1))
        }
    }

}

