package ru.casperix.math.geometry.float64

import ru.casperix.math.geometry.Line2d
import ru.casperix.math.geometry.Line3d
import ru.casperix.math.geometry.tangent
import ru.casperix.math.vector.float64.Vector2d
import ru.casperix.math.vector.rotateCCW

fun Line2d.isFinite(): Boolean {
    return v0.isFinite() && v1.isFinite()
}

fun Line2d.addDimension(z0: Double, z1: Double): Line3d {
    return Line3d(v0.addDimension(z0), v1.addDimension(z1))
}

fun Line2d.normal(): Vector2d {
    return tangent().run { rotateCCW() }
}
