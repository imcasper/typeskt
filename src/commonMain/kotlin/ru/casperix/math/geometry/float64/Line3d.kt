package ru.casperix.math.geometry.float64

import ru.casperix.math.geometry.Line3d


fun Line3d.isFinite(): Boolean {
    return v0.isFinite() && v1.isFinite()
}

