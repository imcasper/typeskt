package ru.casperix.math.vector.float32

import ru.casperix.math.quaternion.float64.QuaternionDouble
import ru.casperix.math.vector.api.AbstractVector4
import ru.casperix.math.vector.float64.Vector4d
import ru.casperix.math.vector.int32.Vector4i
import ru.casperix.misc.ceilToInt
import ru.casperix.misc.toPrecision
import kotlinx.serialization.Serializable
import kotlin.math.*

@Serializable
data class Vector4f(override val x: Float, override val y: Float, override val z: Float, override val w: Float) :
    AbstractVector4<Vector4f, Float> {
    constructor() : this(0f)

    constructor(i: Float) : this(i, i, i, i)

    companion object {
        val NaN = Vector4f(Float.NaN)
        val ZERO = Vector4f(0f)
        val HALF = Vector4f(0.5f)
        val ONE = Vector4f(1f)
        val XYZW = ONE

        val X = Vector4f(1f, 0f, 0f, 0f)
        val Y = Vector4f(0f, 1f, 0f, 0f)
        val Z = Vector4f(0f, 0f, 1f, 0f)
        val W = Vector4f(0f, 0f, 0f, 1f)
    }

    override val xAxis: Vector4f get() = Vector4f(x, 0f, 0f, 0f)
    override val yAxis: Vector4f get() = Vector4f(0f, y, 0f, 0f)
    override val zAxis: Vector4f get() = Vector4f(0f, 0f, z, 0f)
    override val wAxis: Vector4f get() = Vector4f(0f, 0f, 0f, w)


    override fun volume(): Float {
        return x * y * z * w
    }

    override fun distTo(other: Vector4f): Float {
        return (this - other).length()
    }

    override fun lengthOne(): Float {
        return abs(x) + abs(y) + abs(z) + abs(w)
    }

    override fun length(): Float {
        return sqrt(x * x + y * y + z * z + w * w)
    }

    override fun lengthInf(): Float {
        return maxOf(abs(x), abs(y), abs(z), abs(w))
    }

    override fun lengthSquared(): Float {
        return x * x + y * y + z * z + w * w
    }

    override fun absoluteMinimum(): Float {
        return minOf(abs(x), abs(y), abs(z), abs(w))
    }

    override fun absoluteMaximum(): Float {
        return maxOf(abs(x), abs(y), abs(z), abs(w))
    }

    override val sign: Vector4f get() = Vector4f(x.sign, y.sign, z.sign, w.sign)

    override val absoluteValue: Vector4f get() = Vector4f(x.absoluteValue, y.absoluteValue, z.absoluteValue, w.absoluteValue)

    override fun dot(value: Vector4f): Float {
        return (this.x * value.x + this.y * value.y + this.z * value.z + this.w * value.w)
    }

    override fun mod(other: Vector4f): Vector4f {
        return Vector4f(x.mod(other.x), y.mod(other.y), z.mod(other.z), w.mod(other.w))
    }

    override fun upper(other: Vector4f): Vector4f {
        return Vector4f(max(x, other.x), max(y, other.y), max(z, other.z), max(w, other.w))
    }

    override fun lower(other: Vector4f): Vector4f {
        return Vector4f(min(x, other.x), min(y, other.y), min(z, other.z), min(w, other.w))
    }

    fun clamp(min: Vector4f, max: Vector4f): Vector4f {
        return upper(min).lower(max)
    }

    override operator fun plus(position: Vector4f): Vector4f {
        return Vector4f(x + position.x, y + position.y, z + position.z, w + position.w)
    }

    override operator fun minus(position: Vector4f): Vector4f {
        return Vector4f(x - position.x, y - position.y, z - position.z, w - position.w)
    }

    override operator fun div(value: Float): Vector4f {
        return Vector4f(x / value, y / value, z / value, w / value)
    }

    override operator fun div(value: Vector4f): Vector4f {
        return Vector4f(x / value.x, y / value.y, z / value.z, w / value.w)
    }

    override operator fun times(value: Float): Vector4f {
        return Vector4f(x * value, y * value, z * value, w * value)
    }

    override operator fun times(value: Vector4f): Vector4f {
        return Vector4f(x * value.x, y * value.y, z * value.z, w * value.w)
    }

    override operator fun unaryMinus(): Vector4f {
        return Vector4f(-x, -y, -z, -w)
    }

    override operator fun rem(value: Vector4f): Vector4f {
        return Vector4f(x % value.x, y % value.y, z % value.z, w % value.w)
    }

    override operator fun rem(value: Float): Vector4f {
        return Vector4f(x % value, y % value, z % value, w % value)
    }

    override fun greater(other: Vector4f): Boolean {
        return x > other.x && y > other.y && z > other.z && w > other.w
    }

    override fun greaterOrEq(other: Vector4f): Boolean {
        return x >= other.x && y >= other.y && z >= other.z && w >= other.w
    }

    override fun less(other: Vector4f): Boolean {
        return x < other.x && y < other.y && z < other.z && w < other.w
    }

    override fun lessOrEq(other: Vector4f): Boolean {
        return x <= other.x && y <= other.y && z <= other.z && w <= other.w
    }

//	fun addDimension(w: Double): Vector4d {
//		return Vector4d(x, y, z, w)
//	}

    override fun normalize(): Vector4f {
        val len = length().toFloat()
        return Vector4f(x / len, y / len, z / len, w / len)
    }

    fun getQuaternion(): QuaternionDouble {
        return QuaternionDouble(x.toDouble(), y.toDouble(), z.toDouble(), w.toDouble())
    }

    override fun toVector4i(): Vector4i {
        return Vector4i(x.toInt(), y.toInt(), z.toInt(), w.toInt())
    }

    override fun toVector4d(): Vector4d {
        return Vector4d(x.toDouble(), y.toDouble(), z.toDouble(), w.toDouble())
    }

    override fun toVector4f(): Vector4f {
        return this
    }

    fun round(): Vector4f {
        return Vector4f(x.roundToInt().toFloat(), y.roundToInt().toFloat(), z.roundToInt().toFloat(), w.roundToInt().toFloat())
    }

    fun roundToVector4i(): Vector4i {
        return Vector4i(x.roundToInt(), y.roundToInt(), z.roundToInt(), w.roundToInt())
    }

    fun ceilToVector4i(): Vector4i {
        return Vector4i(x.ceilToInt(), y.ceilToInt(), z.ceilToInt(), w.ceilToInt())
    }

    fun toPrecision(precision: Int): String {
        return "${x.toPrecision(precision)}; ${y.toPrecision(precision)}; ${z.toPrecision(precision)}"
    }

    fun isFinite(): Boolean {
        return x.isFinite() && y.isFinite() && z.isFinite() && w.isFinite()
    }

    fun getXYZ(): Vector3f {
        return Vector3f(x, y, z)
    }

    fun getXYW(): Vector3f {
        return Vector3f(x, y, w)
    }

    fun getXZW(): Vector3f {
        return Vector3f(x, z, w)
    }

    fun getYZW(): Vector3f {
        return Vector3f(y, z, w)
    }

    override fun half(): Vector4f {
        return this * 0.5f
    }

    override fun toString(): String {
        return "V3d(x=${x.toPrecision(2)}, y=${y.toPrecision(2)}, z=${z.toPrecision(2)}, w=${w.toPrecision(2)})"
    }

    fun component(index: Int): Float {
        return when (index) {
            0 -> x
            1 -> y
            2 -> z
            3 -> w
            else -> throw Error("Only 4 components enabled")
        }
    }
}


