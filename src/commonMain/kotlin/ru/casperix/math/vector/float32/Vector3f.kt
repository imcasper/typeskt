package ru.casperix.math.vector.float32

import ru.casperix.math.axis_aligned.float32.Box3f
import ru.casperix.math.quaternion.float32.QuaternionFloat
import ru.casperix.math.vector.api.AbstractVector3
import ru.casperix.math.vector.float64.Vector3d
import ru.casperix.math.vector.int32.Vector3i
import ru.casperix.math.vector.uint32.Vector3u
import ru.casperix.misc.ceilToInt
import ru.casperix.misc.format.FormatType
import ru.casperix.misc.toPrecision
import kotlinx.serialization.Serializable
import kotlin.math.*

@Serializable
data class Vector3f(override val x: Float, override val y: Float, override val z: Float) : AbstractVector3<Vector3f, Float> {
    constructor() : this(0f)

    constructor(i: Float) : this(i, i, i)

    companion object {
        val NaN = Vector3f(Float.NaN)
        val ZERO = Vector3f(0.0f)
        val HALF = Vector3f(0.5f)
        val ONE = Vector3f(1.0f)
        val XYZ = ONE

        val X = Vector3f(1f, 0f, 0f)
        val Y = Vector3f(0f, 1f, 0f)
        val Z = Vector3f(0f, 0f, 1f)
        val YZ = Vector3f(0f, 1f, 1f)
        val XZ = Vector3f(1f, 0f, 1f)
        val XY = Vector3f(1f, 1f, 0f)
    }

    override val xAxis: Vector3f get() = Vector3f(x, 0f, 0f)
    override val yAxis: Vector3f get() = Vector3f(0f, y, 0f)
    override val zAxis: Vector3f get() = Vector3f(0f, 0f, z)

    override fun volume(): Float {
        return x * y * z
    }

    override fun distTo(other: Vector3f): Float {
        return (this - other).length()
    }

    override fun lengthOne(): Float {
        return abs(x) + abs(y) + abs(z)
    }

    override fun length(): Float {
        return sqrt(x * x + y * y + z * z)
    }

    override fun lengthInf(): Float {
        return maxOf(abs(x), abs(y), abs(z))
    }

    override fun lengthSquared(): Float {
        return x * x + y * y + z * z
    }

    override fun absoluteMinimum(): Float {
        return minOf(abs(x), abs(y), abs(z))
    }

    override fun absoluteMaximum(): Float {
        return maxOf(abs(x), abs(y), abs(z))
    }

    override val sign: Vector3f get() = Vector3f(x.sign, y.sign, z.sign)

    override val absoluteValue: Vector3f get() = Vector3f(x.absoluteValue, y.absoluteValue, z.absoluteValue)

    override fun dot(value: Vector3f): Float {
        return (this.x * value.x + this.y * value.y + this.z * value.z)
    }

    override fun mod(other: Vector3f): Vector3f {
        return Vector3f(x.mod(other.x), y.mod(other.y), z.mod(other.z))
    }

    override fun upper(other: Vector3f): Vector3f {
        return Vector3f(max(x, other.x), max(y, other.y), max(z, other.z))
    }

    override fun lower(other: Vector3f): Vector3f {
        return Vector3f(min(x, other.x), min(y, other.y), min(z, other.z))
    }

    fun clamp(min: Vector3f, max: Vector3f): Vector3f {
        return upper(min).lower(max)
    }

    fun clamp(area: Box3f): Vector3f {
        return clamp(area.min, area.max)
    }

    override operator fun plus(position: Vector3f): Vector3f {
        return Vector3f(x + position.x, y + position.y, z + position.z)
    }

    override operator fun minus(position: Vector3f): Vector3f {
        return Vector3f(x - position.x, y - position.y, z - position.z)
    }

    override operator fun div(value: Float): Vector3f {
        return Vector3f(x / value, y / value, z / value)
    }

    override operator fun div(value: Vector3f): Vector3f {
        return Vector3f(x / value.x, y / value.y, z / value.z)
    }

    override operator fun times(value: Float): Vector3f {
        return Vector3f(x * value, y * value, z * value)
    }

    override operator fun times(value: Vector3f): Vector3f {
        return Vector3f(x * value.x, y * value.y, z * value.z)
    }

    override operator fun unaryMinus(): Vector3f {
        return Vector3f(-x, -y, -z)
    }

    override operator fun rem(value: Vector3f): Vector3f {
        return Vector3f(x % value.x, y % value.y, z % value.z)
    }

    override operator fun rem(value: Float): Vector3f {
        return Vector3f(x % value, y % value, z % value)
    }

    override fun greater(other: Vector3f): Boolean {
        return x > other.x && y > other.y && z > other.z
    }

    override fun greaterOrEq(other: Vector3f): Boolean {
        return x >= other.x && y >= other.y && z >= other.z
    }

    override fun less(other: Vector3f): Boolean {
        return x < other.x && y < other.y && z < other.z
    }

    override fun lessOrEq(other: Vector3f): Boolean {
        return x <= other.x && y <= other.y && z <= other.z
    }

//	fun addDimension(w: Float): Vector4f {
//		return Vector4d(x, y, z, w)
//	}

    override fun normalize(): Vector3f {
        val len = length().toFloat()
        return Vector3f(x / len, y / len, z / len)
    }

    fun getQuaternion(): QuaternionFloat {
        return QuaternionFloat(x, y, z, 0f)
    }

    override fun toVector3d(): Vector3d {
        return Vector3d(x.toDouble(), y.toDouble(), z.toDouble())
    }

    override fun toVector3i(): Vector3i {
        return Vector3i(x.toInt(), y.toInt(), z.toInt())
    }

    override fun toVector3u(): Vector3u {
        return Vector3u(x.toUInt(), y.toUInt(), z.toUInt())
    }

    override fun toVector3f(): Vector3f {
        return this
    }

    fun round(): Vector3f {
        return Vector3f(x.roundToInt().toFloat(), y.roundToInt().toFloat(), z.roundToInt().toFloat())
    }

    fun roundToVector3i(): Vector3i {
        return Vector3i(x.roundToInt(), y.roundToInt(), z.roundToInt())
    }

    fun ceilToVector3i(): Vector3i {
        return Vector3i(x.ceilToInt(), y.ceilToInt(), z.ceilToInt())
    }

    fun cross(value: Vector3f): Vector3f {
        return Vector3f(this.y * value.z - this.z * value.y, this.z * value.x - this.x * value.z, this.x * value.y - this.y * value.x)
    }

    fun isFinite(): Boolean {
        return x.isFinite() && y.isFinite() && z.isFinite()
    }

    fun getXY(): Vector2f {
        return Vector2f(x, y)
    }

    fun getYZ(): Vector2f {
        return Vector2f(y, z)
    }

    fun getXZ(): Vector2f {
        return Vector2f(x, z)
    }

    fun expand(w: Float): Vector4f {
        return Vector4f(x, y, z, w)
    }

    override fun half(): Vector3f {
        return this * 0.5f
    }


    @Deprecated(message = "Use format instead")
    fun toPrecision(precision: Int): String {
        return "${x.toPrecision(precision)}; ${y.toPrecision(precision)}; ${z.toPrecision(precision)}"
    }

    override fun toString(): String {
        return format()
    }

    fun format(type: FormatType = FormatType.NORMAL, precision: Int = 2): String {
        val sx = x.toPrecision(precision)
        val sy = y.toPrecision(precision)
        val sz = z.toPrecision(precision)
        return when (type) {
            FormatType.DETAIL -> "Vector3f(x=$sx, y=$sy, z=$sz)"
            FormatType.NORMAL -> "V3f($sx, $sy, $sz)"
            FormatType.SHORT -> "(${sx},${sy},${sz})"
        }
    }

    fun component(index: Int): Float {
        return when (index) {
            0 -> x
            1 -> y
            2 -> z
            else -> throw Error("Only 3 components enabled")
        }
    }
}


