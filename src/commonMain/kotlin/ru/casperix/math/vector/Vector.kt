package ru.casperix.math.vector

import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.math.vector.float32.Vector3f
import ru.casperix.math.vector.float64.Vector2d
import ru.casperix.math.vector.float64.Vector3d
import ru.casperix.math.vector.int32.Vector2i
import ru.casperix.math.vector.int32.Vector3i

fun vectorOf(x: Float, y: Float): Vector2f {
    return Vector2f(x, y)
}

fun vectorOf(x: Float, y: Float, z: Float): Vector3f {
    return Vector3f(x, y, z)
}

fun vectorOf(x: Double, y: Double): Vector2d {
    return Vector2d(x, y)
}

fun vectorOf(x: Double, y: Double, z: Double): Vector3d {
    return Vector3d(x, y, z)
}

fun vectorOf(x: Int, y: Int): Vector2i {
    return Vector2i(x, y)
}

fun vectorOf(x: Int, y: Int, z: Int): Vector3i {
    return Vector3i(x, y, z)
}