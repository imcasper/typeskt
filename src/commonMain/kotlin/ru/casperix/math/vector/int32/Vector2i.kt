package ru.casperix.math.vector.int32

import ru.casperix.math.axis_aligned.int32.Box2i
import ru.casperix.math.axis_aligned.int32.Dimension2i
import ru.casperix.math.vector.api.AbstractVector2
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.math.vector.float64.Vector2d
import ru.casperix.misc.format.FormatType
import kotlinx.serialization.Serializable
import kotlin.math.*

@Serializable
data class Vector2i(override val x: Int, override val y: Int) : AbstractVector2<Vector2i, Int> {
    constructor() : this(0)

    constructor(i: Int) : this(i, i)

    companion object {
        val ZERO = Vector2i(0, 0)
        val X = Vector2i(1, 0)
        val Y = Vector2i(0, 1)
        val ONE = Vector2i(1, 1)
        val XY = ONE
    }

    override val xAxis: Vector2i get() = Vector2i(x, 0)

    override val yAxis: Vector2i get() = Vector2i(0, y)

    override fun axisProjection(useXAxis: Boolean): Vector2i {
        return if (useXAxis) xAxis else yAxis
    }

    override fun volume(): Int {
        return x * y
    }

    override fun distTo(other: Vector2i): Int {
        return (this - other).length()
    }

    override fun lengthOne(): Int {
        return abs(x) + abs(y)
    }

    override fun length(): Int {
        return sqrt((x * x + y * y).toFloat()).roundToInt()
    }

    override fun lengthInf(): Int {
        return max(abs(x), abs(y))
    }

    override fun lengthSquared(): Int {
        return x * x + y * y
    }

    override fun absoluteMinimum(): Int {
        return minOf(abs(x), abs(y))
    }

    override fun absoluteMaximum(): Int {
        return maxOf(abs(x), abs(y))
    }

    override val sign: Vector2i get() = Vector2i(x.sign, y.sign)

    override val absoluteValue: Vector2i get() = Vector2i(x.absoluteValue, y.absoluteValue)

    override fun dot(value: Vector2i): Int {
        return (this.x * value.x + this.y * value.y)
    }

    override fun mod(other: Vector2i): Vector2i {
        return Vector2i(x.mod(other.x), y.mod(other.y))
    }

    override fun upper(other: Vector2i): Vector2i {
        return Vector2i(max(x, other.x), max(y, other.y))
    }

    override fun lower(other: Vector2i): Vector2i {
        return Vector2i(min(x, other.x), min(y, other.y))
    }

    fun clamp(min: Vector2i, max: Vector2i): Vector2i {
        return upper(min).lower(max)
    }

    fun clamp(limit: Box2i): Vector2i {
        return clamp(limit.min, limit.max)
    }

    override operator fun plus(position: Vector2i): Vector2i {
        return Vector2i(x + position.x, y + position.y)
    }

    override operator fun minus(position: Vector2i): Vector2i {
        return Vector2i(x - position.x, y - position.y)
    }

    override operator fun div(value: Int): Vector2i {
        return Vector2i(x / value, y / value)
    }

    override operator fun div(value: Vector2i): Vector2i {
        return Vector2i(x / value.x, y / value.y)
    }

    override operator fun times(value: Int): Vector2i {
        return Vector2i(x * value, y * value)
    }

    override operator fun times(value: Vector2i): Vector2i {
        return Vector2i(x * value.x, y * value.y)
    }

    override operator fun unaryMinus(): Vector2i {
        return Vector2i(-x, -y)
    }

    override operator fun rem(value: Vector2i): Vector2i {
        return Vector2i(x % value.x, y % value.y)
    }

    override operator fun rem(value: Int): Vector2i {
        return Vector2i(x % value, y % value)
    }

    override fun greater(other: Vector2i): Boolean {
        return x > other.x && y > other.y
    }

    override fun greaterOrEq(other: Vector2i): Boolean {
        return x >= other.x && y >= other.y
    }

    override fun less(other: Vector2i): Boolean {
        return x < other.x && y < other.y
    }

    override fun lessOrEq(other: Vector2i): Boolean {
        return x <= other.x && y <= other.y
    }

    fun addDimension(z: Int): Vector3i {
        return Vector3i(x, y, z)
    }

    override fun toVector2d(): Vector2d {
        return Vector2d(x.toDouble(), y.toDouble())
    }

    override fun toVector2f(): Vector2f {
        return Vector2f(x.toFloat(), y.toFloat())
    }

    override fun toVector2i(): Vector2i {
        return this
    }

    fun toDimension2i(): Dimension2i {
        return Dimension2i(x, y)
    }

    override fun normalize(): Vector2i {
        val length = sqrt((x * x + y * y).toFloat())
        return Vector2i((x / length).roundToInt(), (y / length).roundToInt())
    }

    operator fun div(value: Double): Vector2d {
        return Vector2d(x / value, y / value)
    }

    fun expand(z: Int): Vector3i {
        return Vector3i(x, y, z)
    }

    override fun half(): Vector2i {
        return this / 2
    }

    override fun toString(): String {
        return format()
    }

    fun format(type: FormatType = FormatType.NORMAL): String {
        return when (type) {
            FormatType.DETAIL -> "Vector2i(x=${x}, y=${y})"
            FormatType.NORMAL -> "V2i(${x}, ${y})"
            FormatType.SHORT -> "${x}x${y}"
        }
    }

    fun component(index: Int): Int {
        return when (index) {
            0 -> x
            1 -> y
            else -> throw Error("Only 2 components enabled")
        }
    }
}


