package ru.casperix.math.vector.int32

import ru.casperix.math.vector.api.AbstractVector4
import ru.casperix.math.vector.float32.Vector4f
import ru.casperix.math.vector.float64.Vector4d
import kotlinx.serialization.Serializable
import kotlin.math.*

@Serializable
data class Vector4i(override val x: Int, override val y: Int, override val z: Int, override val w: Int) :
    AbstractVector4<Vector4i, Int> {
    constructor() : this(0)

    constructor(i: Int) : this(i, i, i, i)

    companion object {
        val ZERO = Vector4i(0)
        val ONE = Vector4i(1)
        val XYZW = ONE

        val X = Vector4i(1, 0, 0, 0)
        val Y = Vector4i(0, 1, 0, 0)
        val Z = Vector4i(0, 0, 1, 0)
        val W = Vector4i(0, 0, 0, 1)
    }

    override val xAxis: Vector4i get() = Vector4i(x, 0, 0, 0)
    override val yAxis: Vector4i get() = Vector4i(0, y, 0, 0)
    override val zAxis: Vector4i get() = Vector4i(0, 0, z, 0)
    override val wAxis: Vector4i get() = Vector4i(0, 0, 0, w)

    override fun volume(): Int {
        return x * y * z * w
    }

    override fun distTo(other: Vector4i): Int {
        return (this - other).length()
    }

    override fun lengthOne(): Int {
        return abs(x) + abs(y) + abs(z) + abs(w)
    }

    override fun length(): Int {
        return sqrt((x * x + y * y + z * z + w * w).toFloat()).roundToInt()
    }

    override fun lengthInf(): Int {
        return maxOf(abs(x), abs(y), abs(z), abs(w))
    }

    override fun lengthSquared(): Int {
        return x * x + y * y + z * z + w * w
    }

    override fun absoluteMinimum(): Int {
        return minOf(abs(x), abs(y), abs(z), abs(w))
    }

    override fun absoluteMaximum(): Int {
        return maxOf(abs(x), abs(y), abs(z), abs(w))
    }

    override fun normalize(): Vector4i {
        return toVector4f().normalize().roundToVector4i()
    }


    override val sign: Vector4i get() = Vector4i(x.sign, y.sign, z.sign, w.sign)

    override val absoluteValue: Vector4i get() = Vector4i(x.absoluteValue, y.absoluteValue, z.absoluteValue, w.absoluteValue)

    override fun dot(value: Vector4i): Int {
        return (this.x * value.x + this.y * value.y + this.z * value.z + this.w * value.w)
    }

    override fun mod(other: Vector4i): Vector4i {
        return Vector4i(x.mod(other.x), y.mod(other.y), z.mod(other.z), w.mod(other.w))
    }

    override fun upper(other: Vector4i): Vector4i {
        return Vector4i(max(x, other.x), max(y, other.y), max(z, other.z), max(w, other.w))
    }

    override fun lower(other: Vector4i): Vector4i {
        return Vector4i(min(x, other.x), min(y, other.y), min(z, other.z), min(w, other.w))
    }

    fun clamp(min: Vector4i, max: Vector4i): Vector4i {
        return upper(min).lower(max)
    }

    operator override fun plus(position: Vector4i): Vector4i {
        return Vector4i(x + position.x, y + position.y, z + position.z, w + position.w)
    }

    operator override fun minus(position: Vector4i): Vector4i {
        return Vector4i(x - position.x, y - position.y, z - position.z, w - position.w)
    }

    operator override fun div(value: Int): Vector4i {
        return Vector4i(x / value, y / value, z / value, w / value)
    }

    operator override fun div(value: Vector4i): Vector4i {
        return Vector4i(x / value.x, y / value.y, z / value.z, w / value.w)
    }

    operator override fun times(value: Int): Vector4i {
        return Vector4i(x * value, y * value, z * value, w * value)
    }

    operator override fun times(value: Vector4i): Vector4i {
        return Vector4i(x * value.x, y * value.y, z * value.z, w * value.w)
    }

    operator override fun unaryMinus(): Vector4i {
        return Vector4i(-x, -y, -z, -w)
    }

    operator override fun rem(value: Vector4i): Vector4i {
        return Vector4i(x % value.x, y % value.y, z % value.z, w % value.w)
    }

    operator override fun rem(value: Int): Vector4i {
        return Vector4i(x % value, y % value, z % value, w % value)
    }

    override fun greater(other: Vector4i): Boolean {
        return x > other.x && y > other.y && z > other.z && w > other.w
    }

    override fun greaterOrEq(other: Vector4i): Boolean {
        return x >= other.x && y >= other.y && z >= other.z && w >= other.w
    }

    override fun less(other: Vector4i): Boolean {
        return x < other.x && y < other.y && z < other.z && w < other.w
    }

    override fun lessOrEq(other: Vector4i): Boolean {
        return x <= other.x && y <= other.y && z <= other.z && w <= other.w
    }

    override fun toVector4f(): Vector4f {
        return Vector4f(x.toFloat(), y.toFloat(), z.toFloat(), w.toFloat())
    }

    override fun toVector4i(): Vector4i {
        return this
    }

    override fun toVector4d(): Vector4d {
        return Vector4d(x.toDouble(), y.toDouble(), z.toDouble(), w.toDouble())
    }

    fun getXYZ(): Vector3i {
        return Vector3i(x, y, z)
    }

    fun getXYW(): Vector3i {
        return Vector3i(x, y, w)
    }

    fun getXZW(): Vector3i {
        return Vector3i(x, z, w)
    }

    fun getYZW(): Vector3i {
        return Vector3i(y, z, w)
    }

    override fun half(): Vector4i {
        return this / 2
    }

    override fun toString(): String {
        return "V4i($x, $y, $z, $w)"
    }

    fun component(index: Int): Int {
        return when (index) {
            0 -> x
            1 -> y
            2 -> z
            3 -> w
            else -> throw Error("Only 4 components enabled")
        }
    }
}


