package ru.casperix.math.vector.api

import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.math.vector.float64.Vector2d
import ru.casperix.math.vector.int32.Vector2i

interface AbstractVector2<Self : Any, Item : Number> : AbstractVectorN<Self, Item> {
    val x: Item
    val y: Item

    val xAxis: Self
    val yAxis: Self
    fun axisProjection(useXAxis: Boolean): Self

    fun toVector2d(): Vector2d
    fun toVector2f(): Vector2f
    fun toVector2i(): Vector2i
}