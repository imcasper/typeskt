package ru.casperix.math.random

import ru.casperix.math.angle.float32.DegreeFloat
import ru.casperix.math.angle.float32.RadianFloat
import ru.casperix.math.angle.float64.DegreeDouble
import ru.casperix.math.angle.float64.RadianDouble
import ru.casperix.math.geometry.MAX_DEGREE
import ru.casperix.math.geometry.PI2
import ru.casperix.math.geometry.fMAX_DEGREE
import ru.casperix.math.geometry.fPI2
import kotlin.random.Random


fun Random.nextRadianFloat(minAngle: Float = 0f, maxAngle: Float = fPI2): RadianFloat {
    return RadianFloat(nextFloat(minAngle, maxAngle))
}

fun Random.nextRadianDouble(minAngle: Double = 0.0, maxAngle: Double = PI2): RadianDouble {
    return RadianDouble(nextDouble(minAngle, maxAngle))
}

fun Random.nextDegreeFloat(minAngle: Float = 0f, maxAngle: Float = fMAX_DEGREE): ru.casperix.math.angle.float32.DegreeFloat {
    return ru.casperix.math.angle.float32.DegreeFloat(nextFloat(minAngle, maxAngle))
}

fun Random.nextDegreeDouble(minAngle: Double = 0.0, maxAngle: Double = MAX_DEGREE): DegreeDouble {
    return DegreeDouble(nextDouble(minAngle, maxAngle))
}
