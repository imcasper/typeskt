package ru.casperix.math.random

import kotlin.random.Random

fun Random.nextFloat(from: Float, to: Float): Float {
    return nextFloat() * (to - from) + from
}

fun Random.nextFloat(until: Float): Float {
    return nextFloat() * until
}

fun Random.nextFloat(range:ClosedRange<Float>): Float {
    return nextFloat(range.start, range.endInclusive)
}
