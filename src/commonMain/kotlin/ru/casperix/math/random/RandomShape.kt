package ru.casperix.math.random

import ru.casperix.math.curve.float32.Arc2f
import ru.casperix.math.curve.float32.BezierCubic2f
import ru.casperix.math.curve.float32.BezierQuadratic2f
import ru.casperix.math.curve.float32.Circle2f
import ru.casperix.math.geometry.*
import ru.casperix.math.geometry.float32.isConvex
import ru.casperix.math.straight_line.float32.LineSegment2f
import ru.casperix.math.vector.float32.Vector2f
import kotlin.random.Random
import kotlin.random.nextInt


fun Random.nextLine2f(minOffset: Vector2f = Vector2f.ZERO, maxOffset: Vector2f = Vector2f.ONE): Line2f {
    return Line2f(nextVector2f(minOffset, maxOffset), nextVector2f(minOffset, maxOffset))
}

fun Random.nextLineSegment2f(minOffset: Vector2f = Vector2f.ZERO, maxOffset: Vector2f = Vector2f.ONE): LineSegment2f {
    return LineSegment2f(nextVector2f(minOffset, maxOffset), nextVector2f(minOffset, maxOffset))
}

fun Random.nextTriangle2f(min: Float, max: Float): Triangle2f {
    return Triangle2f(nextVector2f(min, max), nextVector2f(min, max), nextVector2f(min, max))
}

fun Random.nextTriangle2i(min: Int, max: Int): Triangle2i {
    return Triangle2i(nextVector2i(min, max), nextVector2i(min, max), nextVector2i(min, max))
}

fun Random.nextPolygon2i(vertexRange: IntRange, vertexAmountRange: IntRange): Polygon2i {
    val vertexAmount = vertexAmountRange.random()
    return CustomPolygon((0 until vertexAmount).map {
        nextVector2i(vertexRange)
    })
}

fun Random.nextTriangle2f(minOffset: Vector2f = Vector2f.ZERO, maxOffset: Vector2f = Vector2f.ONE): Triangle2f {
    return Triangle2f(
        nextVector2f(minOffset, maxOffset),
        nextVector2f(minOffset, maxOffset),
        nextVector2f(minOffset, maxOffset),
    )
}


fun Random.nextQuad2f(minOffset: Vector2f = Vector2f.ZERO, maxOffset: Vector2f = Vector2f.ONE, convexOnly: Boolean = true): Quad2f {
    while (true) {
        val next = Quad2f(
            nextVector2f(minOffset, maxOffset),
            nextVector2f(minOffset, maxOffset),
            nextVector2f(minOffset, maxOffset),
            nextVector2f(minOffset, maxOffset)
        )
        if (!convexOnly || next.isConvex()) {
            return Quad2f(
                next.v0,
                next.v1,
                next.v2,
                next.v3,
            )
        }
    }
}


fun Random.nextPolygon2f(
    minOffset: Vector2f = Vector2f.ZERO,
    maxOffset: Vector2f = Vector2f.ONE,
    vertexAmountRange: IntRange = 5..8,
    convexOnly: Boolean = true
): Polygon2f {
    val vertexAmount = nextInt(vertexAmountRange)

    while (true) {

        val vertices = (0 until vertexAmount).map {
            nextVector2f(minOffset, maxOffset)
        }
        val next = CustomPolygon(vertices)
        if (!convexOnly || next.isConvex()) {
            return next
        }
    }
}


fun Random.nextBezierCubic2f(minOffset: Vector2f = Vector2f.ZERO, maxOffset: Vector2f = Vector2f.ONE): BezierCubic2f {
    return BezierCubic2f(
        nextVector2f(minOffset, maxOffset),
        nextVector2f(minOffset, maxOffset),
        nextVector2f(minOffset, maxOffset),
        nextVector2f(minOffset, maxOffset),
    )
}

fun Random.nextBezierQuadratic2f(minOffset: Vector2f = Vector2f.ZERO, maxOffset: Vector2f = Vector2f.ONE): BezierQuadratic2f {
    return BezierQuadratic2f(
        nextVector2f(minOffset, maxOffset),
        nextVector2f(minOffset, maxOffset),
        nextVector2f(minOffset, maxOffset),
    )
}

fun Random.nextCircle2f(
    minCenter: Vector2f = Vector2f.ZERO,
    maxCenter: Vector2f = Vector2f.ONE,
    range: ClosedRange<Float> = 0.5f..1.5f
): Circle2f {
    return Circle2f(
        nextVector2f(minCenter, maxCenter),
        nextFloat(range),
    )
}

fun Random.nextArc2f(
    minCenter: Vector2f = Vector2f.ZERO,
    maxCenter: Vector2f = Vector2f.ONE,
    range: ClosedRange<Float> = 0.5f..1.5f
): Arc2f {
    return Arc2f(nextVector2f(minCenter, maxCenter), nextRadianFloat(), nextRadianFloat(0f, fPI), nextFloat(range))
}