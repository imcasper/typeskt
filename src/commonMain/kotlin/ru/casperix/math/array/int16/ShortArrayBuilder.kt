package ru.casperix.math.array.int16

import ru.casperix.math.array.float32.ArrayBuilder
import kotlin.math.max

class ShortArrayBuilder(initialSize: Int = 16) : ArrayBuilder<ShortArray> {
    private var usage: Int = 0
    private var buffer = ShortArray(initialSize)

    override fun append(data: ShortArray) {
        val end = usage
        length = end + data.size
        data.copyInto(buffer, end)
    }

    fun append(value: Short) {
        val end = usage
        length = end + 1
        buffer[end] = value
    }

    override var length: Int
        get() = usage
        set(value) {
            if (value > buffer.size) {
                val next = ShortArray(max(value, buffer.size * 2))
                buffer.copyInto(next)
                buffer = next
            }
            usage = value
        }

    override fun build(): ShortArray {
        return buffer.sliceArray(0 until usage)
    }
}