package ru.casperix.math.array

interface MutableArray1D<Item : Any> : Array1D<Item> {
    operator fun set(index: Int, value: Item)
}