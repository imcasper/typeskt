package ru.casperix.math.array.float32

interface ArrayBuilder<TargetArray> {
    fun append(data:TargetArray)
    var length:Int
    fun build():TargetArray
}