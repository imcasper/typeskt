package ru.casperix.math.array.float32

import kotlin.math.max

class UIntArrayBuilder(initialSize: Int = 16) : ArrayBuilder<UIntArray> {
     private var usage: Int = 0
    private var buffer = UIntArray(initialSize)

    override fun append(data: UIntArray) {
        val end = usage
        length = end + data.size
        data.copyInto(buffer, end)
    }

    override var length: Int
        get() = usage
        set(value) {
            if (value > buffer.size) {
                val next = UIntArray(max(value, buffer.size * 2))
                buffer.copyInto(next)
                buffer = next
            }
            usage = value
        }

    override fun build(): UIntArray {
        return buffer.sliceArray(0 until usage)
    }
}