package ru.casperix.math.array.float64

import ru.casperix.math.array.IndexedMap2D
import ru.casperix.math.array.MutableMap2D
import ru.casperix.math.axis_aligned.int32.Dimension2i
import ru.casperix.math.vector.int32.Vector2i


@kotlinx.serialization.Serializable
data class DoubleMap2D(override val dimension: Vector2i, val array: DoubleArray) : IndexedMap2D<Double>, MutableMap2D<Double> {

	override fun setByIndex(index: Int, value: Double) {
		array[index] = value
	}

	override fun getByIndex(index: Int): Double {
		return array[index]
	}

	companion object {
		fun create(dimension: Vector2i): DoubleMap2D {
			return DoubleMap2D(dimension, DoubleArray(dimension.volume()))
		}

		fun create(dimension: Vector2i, builder: (index: Int) -> Double): DoubleMap2D {
			return DoubleMap2D(dimension, DoubleArray(dimension.volume()) { builder(it) })
		}

		fun createByXY(dimension: Vector2i, builder: (pos: Vector2i) -> Double): DoubleMap2D {
			var x = -1
			var y = 0
			val array = DoubleArray(dimension.volume()) {
				x++
				if (x == dimension.x) {
					x = 0
					y++
				}
				builder(Vector2i(x, y))
			}

			return DoubleMap2D(dimension, array)
		}
	}

	init {
		if (array.size != dimension.volume()) throw Error("Invalid array size. Need: ${dimension.volume()}")
	}

	override fun equals(other: Any?): Boolean {
		if (this === other) return true
		if (other == null || this::class != other::class) return false

		other as DoubleMap2D

		if (!array.contentEquals(other.array)) return false
		if (dimension != other.dimension) return false

		return true
	}

	override fun hashCode(): Int {
		var result = array.contentHashCode()
		result = 31 * result + dimension.hashCode()
		return result
	}


}