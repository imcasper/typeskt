package ru.casperix.math.mesh.float32

import ru.casperix.math.straight_line.float32.LineSegment2f
import ru.casperix.math.vector.float32.Vector2f

data class MeshEdge(val shape: LineSegment2f, val tag: MeshEdgeTag? = null) {
    constructor(start: Vector2f, finish: Vector2f, tag: MeshEdgeTag? = null) : this(LineSegment2f(start, finish), tag)
}

//{
//    val aabbox: Box2f by lazy {
//        Box2f.byCorners(segment.start, segment.finish)
//    }
//
//    constructor(start: Vector2f, finish: Vector2f) : this(LineSegment2f(start, finish))
//
//    private val cacheHashCode: Int by lazy { segment.start.hashCode() + segment.finish.hashCode() }
//    private val cacheInvert: LineSegment2f by lazy { segment.invert() }
//
//    override fun equals(other: Any?): Boolean {
//        val otherEdge = other as? Edge ?: return false
//        return otherEdge.segment == segment || otherEdge.segment == cacheInvert
//    }
//
//    override fun hashCode(): Int {
//        return cacheHashCode
//    }
//
//    fun invert(): Edge = segment.run {
//        return Edge(finish, start)
//    }
//}