package ru.casperix.math.mesh.float32

import ru.casperix.math.straight_line.float32.LineSegment2f
import ru.casperix.math.vector.float32.Vector2f

data class UnorderedSegment(val basis: LineSegment2f) {
    constructor(a: Vector2f, b: Vector2f) : this(LineSegment2f(a, b))

    override fun equals(other: Any?): Boolean {
        val o = other as? UnorderedSegment ?: return false
        return o.basis == basis || o.basis == basis.invert()
    }

    override fun hashCode(): Int {
        return basis.start.hashCode() + basis.finish.hashCode()
    }
}